var AudioModel = Backbone.Model.extend({
	fileAttribute : 'attachment',
	defaults : {
		"attachment" : '',
		"trackName" : ''
	},
	validate : function(attrs) {
		var errors = [];
		if (!attrs.attachment) {
			errors.push({
				field : 'attachment',
				message : 'Please select file.'
			});
		}
		if (!attrs.trackName) {
			errors.push({
				field : 'trackName',
				message : 'Please enter track name.'
			});
		}
		return errors.length > 0 ? errors : false;
	}
});