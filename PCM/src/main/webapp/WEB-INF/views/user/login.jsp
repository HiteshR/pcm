<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page session="false"%>
<html class="no-js" lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta description="" />
<title>Peter Chung | Log In</title>
<link rel="icon" type="image/x-icon"
	href="<c:url value="/img/pcm.ico"/>" />
<link rel="stylesheet" type="text/css"
	href="<c:url value="/css/foundation.min.css"/>" />
<link rel="stylesheet" type="text/css"
	href="<c:url value="/css/styles.css"/>">
<style>
body {
	background-color: #ebd7c2;
}

h1 {
	font-family: Strangelove;
}

h1 span {
	color: #483247;
}

input[type="text"],input[type="password"] {
	border-radius: 5px;
	margin-bottom: 5px;
	border: 1px dotted #066682;
}

.alert-box.success {
	background-color: #bd1e67;
	border: 1px solid #483247;
}

p {
	text-align: right;
	font-family: 'Modern20';
}

button {
	background-color: #066682;
	font-family: 'Strangelove';
	font-size: 2em;
	padding: 10px 20px;
}

.admin-login {
	margin-top: 10%;
}

.login-img img {
	width: 500px;
}

/*Small screens*/
@media only screen {
	.login-container {
		
	}
	.login-img img {
		width: 100%;
	}
}
/* Define mobile styles */
@media only screen and (max-width: 40em) {
	.login-container {
		margin-top: 15%;
	}
	.login-img img {
		width: 100%;
	}
}
/* max-width 640px, mobile-only styles, use when QAing mobile issues */
/* Medium screens*/
@media only screen and (min-width: 40.063em) {
	.login-container {
		/*margin-top: 15%;*/
		
	}
	.login-img {
		text-align: center !important;
	}
	.login-img img {
		width: 75%;
	}
}
/* min-width 641px, medium screens */
@media only screen and (min-width: 40.063em) and (max-width: 64em) {
	.login-container {
		/*margin-top: 15%;*/
		margin-top: 0;
	}
	.login-img {
		text-align: center !important;
	}
	.login-img img {
		width: 75%;
	}
}
/* min-width 641px and max-width 1024px, use when QAing tablet-only issues */
</style>
</head>
<body>
	<div>
		<div class="row admin-login">
			<div class="large-7 medium-12 small-12 columns login-img">
				<img src="img/PCMlogo.2.png" />
			</div>
			<div class="large-5 medium-12 small-12 columns login-container">
				<form:form commandName="loginUser" action="login" method="post">
					<h1>
						Admin <span>|</span> log in
					</h1>
					${errorMSG}
					<form:input path="email" placeholder="email" />
					<form:errors path="email" cssClass="error" />
					<form:password path="password" placeholder="password" />
					<form:errors path="password" cssClass="error" />
					<p>Forgot My Password</p>
					<button class="right radius login" type="submit">Log In</button>
				</form:form>
			</div>
		</div>
	</div>
	<script src="<c:url value="/js/jquery.js"/>"></script>
	<script src="<c:url value="/js/foundation.js"/>"></script>
	<script>
		$(document).foundation();
	</script>
</body>
</html>
