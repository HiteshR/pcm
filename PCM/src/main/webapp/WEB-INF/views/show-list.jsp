<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page session="false"%>
<html>
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta description="" />
<title>Peter Chung | Log In</title>
<link rel="icon" type="image/x-icon" href="<c:url value="img/pcm.ico"/>" />
<link rel="stylesheet" type="text/css"
	href="<c:url value="css/foundation.min.css"/>">
<link rel="stylesheet" type="text/css"
	href="<c:url value="css/styles.css"/>">
<link rel="stylesheet" type="text/css"
	href="<c:url value="css/jquery.datetimepicker.css"/>">
</head>
<body>
	<div class="row hide-for-large-up logoRow">
		<div>
			<img src="<c:url value="img/adminLogo.3.png"/>">
		</div>
	</div>
	<header>
		<nav class="top-bar" data-topbar role="navigation">
			<ul class="title-area">
				<li class="name"><img class="show-for-large-up"
					src="img/adminLogo.3.png"></a></li>
				<!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
				<li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
			</ul>

			<section class="top-bar-section">
				<!-- Right Nav Section -->
				<ul>
					<li><a href="<c:url value="about"/>">about</a></li>
					<li><a href="<c:url value="audio"/>">audio</a></li>
					<!-- <li class="hide"><a href="#">lyrics</a></li>-->
					<li><a href="<c:url value="video"/>">video</a></li>
					<li><a href="<c:url value="image"/>">photos</a></li>
					<li><a href="<c:url value="show"/>">shows</a></li>
					<li><a href="#">Newsletter</a></li>
					<li class="has-dropdown"><a href="#">Settings</a>
						<ul class="dropdown">
							<li><a href="<c:url value="account"/>">Acount</a></li>
							<li class="active"><a href="<c:url value="logout"/>">Logout</a></li>
						</ul></li>
				</ul>
				<!-- Left Nav Section -->
			</section>
		</nav>
	</header>
	<div class="content"></div>
	<script type="text/template" id="show_template">
	<section class="event-tab tabs">  
        <div class="row">
          <div class="large-12 medium-12 small-12 columns">
            <h3 class="left">
              Edit Shows
            </h3>
          </div>
        </div>
        <div class="row">
          <div class="large-12 medium-12 small-12 columns table">
             <button class="right radius submit modal add-new-show"> add +</button>
            <div class="large-12 medium-12 small-12 columns show-list"></div>
          </div>
        </div>
      </section>
		<div id="eventModal" class="reveal-modal full" data-reveal>
		<h1>event</h1>
			<div class="row">
				<div class="large-12 medium-12 small-12 columns">
					<ul	class="small-block-grid-2 medium-block-grid-2 large-block-grid-3">
						<li><input type="text" id="city" placeholder="city">
							<div class="cityError control-group">
								<span class="help-inline"></span>
							</div>
						</li>
						<li><input type="text" id="venue" placeholder="venue">
							<div class="venueError control-group">
								<span class="help-inline"></span>
							</div>
						</li>
						<li><input type="text" id="detail" placeholder="detail">
							<div class="detailError control-group">
								<span class="help-inline"></span>
							</div>
						</li>
						<li><input type="text" id="date" placeholder="date" readonly>
							<div class="dateError control-group">
								<span class="help-inline"></span>
							</div>
						</li>
						<li><select id="timezone"></select>
							<div class="timezoneError control-group">
								<span class="help-inline"></span>
							</div>
						</li>
					<ul>
						<button class="right radius login save-update-event">Submit</button>
				</div>
			</div>
			<a class="close-reveal-modal">&#215;</a>
		</div>
	</script>
	<footer>
		<p>footer</p>
	</footer>
	<script src="<c:url value="js/jquery.js"/>"></script>
	<script src="<c:url value="js/jquery.datetimepicker.js"/>"></script>
	<script src="<c:url value="js/sticky-footer.js"/>"></script>
	<script src="<c:url value="js/foundation.js"/>"></script>
	<script src="<c:url value="js/foundation.topbar.js"/>"></script>
	<script src="<c:url value="js/foundation.reveal.js"/>"></script>
	<script src="<c:url value="js/foundation.dropdown.js"/>"></script>

	<script type="text/javascript"
		src="<c:url value="/js/underscore-min.js"/>"></script>
	<script type="text/javascript"
		src="<c:url value="/js/backbone-min.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/js/handlebars.js"/>"></script>

	<script type="text/javascript"
		src="<c:url value="/js/model/ShowModel.js"/>"></script>
	<script type="text/javascript"
		src="<c:url value="/js/view/ShowView.js"/>"></script>
	<script type="text/javascript">
		var showModel = Backbone.Model.extend();
		var showView = new ShowView({
			model : new showModel({
				"baseUrl" : "${pageContext.servletContext.contextPath}"
			})
		});
		showView.render();
		$(document).foundation();
	</script>
</html>
