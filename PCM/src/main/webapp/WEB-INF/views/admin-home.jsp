<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page session="false"%>
<html>
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta description="" />
<title>Peter Chung | Log In</title>
<link rel="icon" type="image/x-icon" href="<c:url value="img/pcm.ico"/>" />
<link rel="stylesheet" type="text/css"
	href="<c:url value="css/foundation.min.css"/>">
<link rel="stylesheet" type="text/css"
	href="<c:url value="css/styles.css"/>">
<style type="text/css">
body {
	background-color: #ebd7c2;
}

.top-bar {
	background-color: #066682;
}

.top-bar.expanded .title-area {
	background-color: #066682;
}

.top-bar-section ul {
	float: right;
	text-align: right;
}

.top-bar-section ul li>a {
	font-family: 'Strangelove';
	font-size: 1.8em;
	letter-spacing: 1.5px;
}

.top-bar-section




 




li








:not




 




(
.has-form




 




)
a








:not




 




(
.button




 




){
background-color








:




 




#066682








;
}
.top-bar-section li:not (.has-form ) a:not (.button ):hover {
	background-color: #1b1464;
}

footer {
	background-color: #066682;
	width: 100%;
	height: 100px;
	color: #FFF;
	padding: 30px 50px 0px 50px;
}

.content {
	margin: 3% 0;
}

.logoRow {
	margin: 0px;
	background-color: #066682;
	text-align: center;
	padding: 20px 0px;
	min-width: 100%;
}

.logoRow div img {
	width: 75%;
}

.name img {
	width: 200px;
	padding: 10px 0 0 15px;
}

.edit-box {
	border-left-color: #483247;
	border-left-style: solid;
	border-left-width: 7px;
	border-right-color: #483247;
	border-right-style: solid;
	border-right-width: 7px;
	border-top-color: #483247;
	border-top-style: solid;
	border-top-width: 3px;
	border-bottom-color: #483247;
	border-bottom-style: solid;
	border-bottom-width: 3px;
	height: auto;
	width: 100%;
	border-radius: 10px;
}

button,.login {
	background-color: #bd1e67;
	font-family: 'Strangelove';
	font-size: 2em;
	padding: 10px 20px;
	text-transform: uppercase;
}

button,.login:hover {
	background-color: transparent;
	border: 1px solid #bd1e67;
	color: #483247;
}

}
/*Small screens*/
@media only screen {
	.top-bar-section ul {
		float: right;
		text-align: center;
	}
} /* Define mobile styles */
@media only screen and (max-width: 40em) {
	.top-bar-section ul {
		float: right;
		text-align: center;
	}
}
/* max-width 640px, mobile-only styles, use when QAing mobile issues */
/*Medium screens*/
@media only screen and (min-width: 40.063em) {
	.logoRow div img {
		width: 50%;
	}
} /* min-width 641px, medium screens */
@media only screen and (min-width: 40.063em) and (max-width: 64em) {
	.logoRow div img {
		width: 50%;
	}
	.top-bar-section {
		text-align: center;
	}
}
/* min-width 641px and max-width 1024px, use when QAing tablet-only issues */
</style>
</head>
<body>
	<div class="row hide-for-large-up logoRow">
		<div>
			<img src="<c:url value="img/adminLogo.3.png"/>">
		</div>
	</div>
	<header>
		<nav class="top-bar" data-topbar role="navigation">
			<ul class="title-area">
				<li class="name"><img class="show-for-large-up"
					src="<c:url value="img/adminLogo.3.png"/>"></a></li>
				<!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
				<li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
			</ul>

			<section class="top-bar-section">
				<!-- Right Nav Section -->
				<ul>
					<li><a href="#">about</a></li>
					<li><a href="#">audio</a></li>
					<!-- <li><a href="#">lyrics</a></li>-->
					<li><a href="#">video</a></li>
					<li><a href="#">photos</a></li>
					<li><a href="#">shows</a></li>
					<li><a href="#">Messages</a></li>
					<li><a href="#">Settings</a></li>

				</ul>

				<!-- Left Nav Section -->

			</section>
		</nav>
	</header>
	<div class="content">
		<div class="about-tab">
			<div class="row">
				<div class="large-12 medium-12 small-12 columns">
					<h3 class="left">Edit About</h3>
				</div>
			</div>
			<div class="row">
				<div class="large-12 medium-12 small-12 columns">
					<form:form commandName="about" method="post" action="about">
						<form:textarea path="content" cols="80" rows="10" />
						<input type="submit" value="save">
					</form:form>
					<div class="edit-box">edit about here</div>
					<br>
					<button class="right radius login">Submit</button>
				</div>
			</div>
		</div>
	</div>
	<footer>
		<p>footer</p>
	</footer>

	<script src="<c:url value="js/jquery.js"/>"></script>
	<script src="<c:url value="js/sticky-footer.js"/>"></script>
	<script src="<c:url value="js/foundation.js"/>"></script>
	<script src="<c:url value="js/foundation.topbar.js"/>"></script>
	<script src="<c:url value="js/foundation.reveal.js"/>"></script>
	<script src="<c:url value="/js/ckeditor/ckeditor.js"></c:url>"></script>
	<script src="<c:url value="/js/ckeditor/adapters/jquery.js"></c:url>"></script>
	<script>
		$(document).foundation();
	</script>
	<script>
		CKEDITOR.disableAutoInline = true;

		$(document).ready(function() {
			$('#content').ckeditor(); // Use CKEDITOR.replace() if element is <textarea>.

		});

		function setValue() {
			$('#content').val($('input#val').val());
		}
		/*$("#save").click(function() {
			var val = CKEDITOR.instances['editor1'].getData();
			alert(val);
		});*/
	</script>
</html>
