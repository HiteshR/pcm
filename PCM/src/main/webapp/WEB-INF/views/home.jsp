<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page session="false"%>
<html class="no-js" lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta description="mustical talentent by san francisco's peter chung" />
<title>Peter Chung | Music</title>
<link rel="icon" type="image/x-icon"
	href="<c:url value="/img/pcm.ico"/>" />
<link rel="stylesheet" type="text/css"
	href="<c:url value="/css/foundation.min.css"/>">
<link rel="stylesheet" type="text/css"
	href="<c:url value="/css/slick.css"/>">
<link rel="stylesheet" type="text/css"
	href="<c:url value="/css/styles.css"/>">
<style>
</style>
</head>
<body>
	<div class="main-container">
		<div class="show-for-large-up move-down">
			<img src="<c:url value="/img/down.png"/>" width="25px">
		</div>
		<section id="home">
			<div>
				<div class="row" style="" id="nav-container">
					<ul
						class="large-block-grid-5 medium-block-grid-5 small-block-grid-5">
						<li><a href="#about"> <img class="navIconz"
								data-alt-src="img/about.1.png"
								src="<c:url value="/img/about.png"/>">
						</a></li>
						<li><a href="#lyrics"> <img class="navIconz"
								data-alt-src="img/lyrics.1.png"
								src="<c:url value="/img/lyrics.png"/>">
						</a></li>
						<li><a href="#videos"> <img class="navIconz"
								data-alt-src="img/video.1.png"
								src="<c:url value="/img/video.png"/>">
						</a></li>
						<li id="showIcon">
							<div class=" showOverlay">
								<a href="#events"> <img class="navIconz"
									data-alt-src="img/shows.1.png"
									src="<c:url value="/img/shows.png"/>">
								</a>
							</div>
						</li>
						<li><a href="#contact"> <img class="navIconz"
								data-alt-src="img/contact.1.png"
								src="<c:url value="/img/contact.2.png"/>">
						</a></li>
					</ul>
				</div>
				<div class="row" id="logo-container">
					<div class="large-12 medium-12 small-12 columns">
						<img src="<c:url value="/img/PCMlogo.2.png"/>" />
					</div>
				</div>
				<div class="row socialmedia-container">
					<div class="large-12 medium-12 small-12 columns">
						<ul id="social-nav">
							<li><a href="#contact">newsletter |</a></li>
							<li><a href="https://www.facebook.com/peterchungmusic"
								target="_blank">facebook |</a></li>
							<li><a href="#">itunes |</a></li>
							<li><a href="https://twitter.com/peterchungmusic"
								target="_blank">twitter |</a></li>
							<li><a href="#">instagram</a></li>
						</ul>
					</div>
				</div>
			</div>
		</section>
		<div class="sidescroll hide">
			<a href="#home"><img class="navIconz"
				data-alt-src="img/home.1.png" src="<c:url value="/img/home.png"/>"></a>
			<a href="#about"><img class="navIconz"
				data-alt-src="img/about.1.png" src="<c:url value="/img/about.png"/>"></a>
			<a href="#lyrics"><img class="navIconz"
				data-alt-src="img/lyrics.1.png"
				src="<c:url value="/img/lyrics.png"/>"></a> <a href="#videos"><img
				class="navIconz" data-alt-src="img/video.1.png"
				src="<c:url value="/img/video.png"/>"></a> <a href="#events"><img
				class="navIconz" data-alt-src="img/shows.1.png"
				src="<c:url value="/img/shows.png"/>"></a> <a href="#contact"><img
				class="navIconz" data-alt-src="img/contact.1.png"
				src="<c:url value="/img/contact.2.png"/>"></a>
		</div>
		<section id="about" class="main-sections">
			<div class="section">
				<div class="row">
					<div class="large-12 medium-12 small-12 columns title right">
						<h1>about</h1>
					</div>
				</div>
				<div class="row">
					<div class="large-12 medium-12 small-12 columns content-container">
						<div class="about-icons show-for-large-up">
							<div>
								<img src="<c:url value="/img/up.png"/>" width="25px">
							</div>
							<br>
							<div style="clear: both;">
								<img src="<c:url value="/img/down.png"/>" width="25px">
							</div>
						</div>
						<div class="content">${about.content}</div>
					</div>
				</div>
			</div>
		</section>
		<div class="songs-container ">
			<div class="row">
				<div class="large-12 medium-12 small-12 columns"
					id="listen-container">
					<div id="click-listen">
						<a><img src="<c:url value="/img/listen.png"/>"></a>
						<div id="player-container" class="hide">
							<audio controls style="width: 100%;">
								<c:forEach items="${audios}" var="audio">
									<source src="<c:url value="/music/${audio.fileName}"/>" type="audio/mpeg">
								</c:forEach>
								Your browser does not support the audio element.
							</audio>
						</div>
					</div>
				</div>
			</div>
		</div>
		<section id="lyrics" class="lyrics-title" style="min-height: 1700px">
			<div class="lyrics-container">
				<div class="G-overlay show-for-large-up right">
					<img src="<c:url value="/img/guitar.png"/>" width="525">
				</div>
				<div class="row" style="margin-bottom: 5%;">
					<div class="large-12 medium-12 small-12 columns title right">
						<h1>lyrics</h1>
					</div>
				</div>
				<!--  <div class="show-for-large-up" style="position:absolute; min-width:100%;">
                        <div class="row" style="margin:0px padding:0px;border:1px solid green;">
                            <div class="large-6 columns">
                                <div><a><img src="img/about.png" width="75"></a></div>
                                <div><p><strong>WAITING  //</strong>Waiting for someone/ Waiting for something to be done/ I’ve been waiting for a while now/ For that good love to come/ Watching the sunrise/ Watching the clouds to blue to gray/ I’ve been waiting for the wind to blow/ Good love my way/ What good’s a heart/ What good’s a heart with no one to love?/ Will I be waiting for forever/ I’ll find out soon enough/ Always analyzing/ What and when did I do wrong/ Cause I held love in my hands once/ And in a second it was gone/ What good’s a heart/ What good’s a heart with no one to love/ Will I be waiting for forever darling/ We’ll find out soon enough/ Maybe love will take a while/ Maybe love might find me soon/ Maybe waiting slows me down/ And I can change my blues to our maroons/ I’ll find my way/ I’ll find your way/ Watching the sunrise/ Watching the clouds turn blue and gray/ You’ve been waiting for too long but don’t worry/ I’m on my way</p></div>
                            </div>
                            <div class="large-6 columns" style="text-align:right;margin-top:100px">
                            <div><a><img src="img/about.png" width="75"></a></div>
                                <div><p><strong>BACK  //</strong>It’s been a while since I last saw you/ 21 days to be exact/ Though the road is where I lay my head/All of the streetlights say come back/ To you my love/ Oh my love/ How many hours have I been driving/ So many places I’ve lost track/ Though the road is where my eyes look to/ My minds imagining me back/ With you my love/ Oh my love/ I’m coming home (I’m on my way)/ I’m on my way back home/  How many hours have I been driving/ So many places I’ve lost track/ But you don’t have to hold your head my dear/ Cause very soon I will be back/ To you my love/ Oh my love</p></div></div>
                        </div>
                        <div class="row">
                        <div class="large-12 columns show-for-large-up">
                            <p><strong>WE CAN GO WHEREVER WE WANT  //</strong>We can go wherever we want/ Let’s go to the city/ Wander museums/ and stare at Picassos/ We can go wherever we want/ Let’s go to the country/ And hike in the valleys/ And I’ll bring the wine/  I’ve been thinking of places that we’ve never been/ And things we could do that you would enjoy/ And I’d hold you/ And you’d hold me/ We can go wherever we want/ Let’s drive to Los Angeles/ Hold hands in the car/ Find out who we are/ Let’s buy things we don’t need/ We can go wherever we want/ Let’s fly to Boston/ Let’s get lost in the subways and snow/ I’ve been thinking of places that we’ve never been/ And things we could do that you would enjoy/ And I’d hold you/ And you’d hold me/ I’ve been thinking of places that we’ve never been/ And things we could do and how I could kiss you/ And hold you/ And you’d hold me/ We can go wherever we want/ But you say let’s stay here/ Baby just lay here/ We’ll stay here/ My love</p>
                        </div>
                        </div>
                        <div class="row">
                            <div class="large-6 columns"></div>
                            <div class="large-6 columns"></div>
                        </div>
                        <div class="row">
                            <div class="large-6 columns"></div>
                            <div class="large-6 columns"></div>
                        </div>
                    </div>-->
				<div>
					<div class="row show-for-large-up" style="margin-right: 0px;">
						<div class="large-12 columns show-for-large-up">
							<p>
								<strong>WAITING //</strong>Waiting for someone/ Waiting for
								something to be done/ I’ve been waiting for a while now/ For
								that good love to come/ Watching the sunrise/ Watching the
								clouds to blue to gray/ I’ve been waiting for the wind to blow/
								Good love my way/ What good’s a heart/ What good’s a heart with
								no one to love?/ Will I be waiting for forever/ I’ll find out
								soon enough/ Always analyzing/ What and when did I do wrong/
								Cause I held love in my hands once/ And in a second it was gone/
								What good’s a heart/ What good’s a heart with no one to love/
								Will I be waiting for forever darling/ We’ll find out soon
								enough/ Maybe love will take a while/ Maybe love might find me
								soon/ Maybe waiting slows me down/ And I can change my blues to
								our maroons/ I’ll find my way/ I’ll find your way/ Watching the
								sunrise/ Watching the clouds turn blue and gray/ You’ve been
								waiting for too long but don’t worry/ I’m on my way
							</p>
						</div>
					</div>
					<div class="row show-for-large-up" style="margin-right: 0px;"
						data-equalizer>
						<div class="large-6 columns show-for-large-up"
							data-equalizer-watch>
							<p>
								<strong>BACK //</strong>It’s been a while since I last saw you/
								21 days to be exact/ Though the road is where I lay my head/All
								of the streetlights say come back/ To you my love/ Oh my love/
								How many hours have I been driving/ So many places I’ve lost
								track/ Though the road is where my eyes look to/ My minds
								imagining me back/ With you my love/ Oh my love/ I’m coming home
								(I’m on my way)/ I’m on my way back home/ How many hours have I
								been driving/ So many places I’ve lost track/ But you don’t have
								to hold your head my dear/ Cause very soon I will be back/ To
								you my love/ Oh my love
							</p>
						</div>
						<div class="large-6 columns show-for-large-up"
							data-equalizer-watch>
							<p>
								<strong>COMING HOME //</strong>The road goes on for days/ The
								clouds cry their tears of gray/ It’s getting colder the days
								getting dark/ But this stormy weather can’t keep me apart from
								you/ The music on the stereo sings “You’ve got so far to go”/
								I’m on my way/ The wind whispers I’m getting close signs with
								city names I know/ I’m on my way/ The road goes on for miles/
								And I’ve been thinking ’bout you for quite some time now/ It’s
								getting colder the days getting dark/ But this stormy weather
								can’t keep me apart from you/ The music on the stereo now sings
								“I wish I was homeward bound”/ I’m on my way/ The wind whispers
								I’m getting close familiar buildings, shops and roads/ I’m on my
								way/ I’m coming home
							</p>
						</div>
					</div>
					<div class="row show-for-large-up" style="margin-right: 0px;">
						<div class="large-12 columns show-for-large-up">
							<p>
								<strong>HESITATION //</strong>The thought of you/ Just the
								thought of you/ Now the winter’s not so cold/ I’m counting down
								the days/ ’til my love comes home/ When I hear your voice/ I
								feel your voice/ Vibrating through my soul/ I’m counting down
								the days/ ’til my love comes home/ And I’ll sing Ooh come home/
								What does this hesitation mean/ If I try harder I’ll make scene/
								But when the prize is worth the risk/ I’ll take a chance and go
								for it/ What are her eyes telling me/ If I look harder I might
								blink/ But when her lips are worth my wait/ I can’t I won’t
								hesitate/ She’s so beautifully dressed/ My stomach’s in a mess/
								Can I pull this off/ Pull of these butterflies/ What does this
								conversation mean/ If I keep quiet she might think/ I’m just
								here to waste her time/ But I’m just lost in her eyes/ She’s so
								beautifully dressed/ My stomach’s in a mess/ Can I pull this
								off/ Pull of these butterflies/ My hands in my pockets/ My eyes
								on the table/ She wants me she wants me/ I know I am capable/
								Eyes switch to dress/ Down to shoes down to floor/ She wants me
								she wants me/ But I want her more/ What does this conversation
								mean/ If I keep quiet she might think/ That I’m just here to
								waste her time/ But I’m just lost in her eyes
							</p>
						</div>
					</div>
					<div class="row show-for-large-up" style="margin-right: 0px;"
						data-equalizer>
						<div class="large-4 columns show-for-large-up"
							data-equalizer-watch>
							<p>
								<strong>SAND //</strong>I tried/ I tried my best to hold on/
								Hold on tight to you/ All in vain/ You slipped through my hands
								like a fistful of sand/ Drifting with the tides of the sea/ I’m
								trying to find some of my mind/ By fixing my faults from the
								past/ But it’s all in my head and the love that I bled/ Has
								shriveled and died in the cold/ You slipped through my hands
								like a fistful of sand/ Drifting with the tides of the sea/ You
								were in my hands…
							</p>
						</div>
						<div class="large-4 columns show-for-large-up"
							data-equalizer-watch>
							<p>
								<strong>COUNTING DOWN THE DAYS //</strong>The thought of you/
								Just the thought of you/ Now the winter’s not so cold/ I’m
								counting down the days/ / ’til my love comes home/ When I hear
								your voice/ I feel your voice/ Vibrating through my soul/ I’m
								counting down the days/ ’til my love comes home
							</p>
						</div>
						<div class="large-4 columns show-for-large-up"
							data-equalizer-watch>
							<p>
								<strong>FOREVER MEANS FOREVER //</strong>I love you so much/
								I’ll love you forever/ I mean forever/ I love you so much/ I’ll
								kiss you forever/ I’ll hold you forever/ When our hearts grow
								tired/ I’ll be sure to sing you this song/ When we lose control/
								I’ll give in/ I’ll be wrong/ Cause forever means forever/ I love
								you so much/ I’ll be yours forever/ I am yours forever/ When our
								hearts grow tired/ I’ll be sure to sing you this song/ When we
								lose control/ I’ll give in/ I’ll be wrong/ Cause forever means
								forever/ Forever means forever
							</p>
						</div>
					</div>
					<div class="row show-for-large-up" style="margin-right: 0px;">
						<div class="large-12 columns show-for-large-up">
							<p>
								<strong>WE CAN GO WHEREVER WE WANT //</strong>We can go wherever
								we want/ Let’s go to the city/ Wander museums/ and stare at
								Picassos/ We can go wherever we want/ Let’s go to the country/
								And hike in the valleys/ And I’ll bring the wine/ I’ve been
								thinking of places that we’ve never been/ And things we could do
								that you would enjoy/ And I’d hold you/ And you’d hold me/ We
								can go wherever we want/ Let’s drive to Los Angeles/ Hold hands
								in the car/ Find out who we are/ Let’s buy things we don’t need/
								We can go wherever we want/ Let’s fly to Boston/ Let’s get lost
								in the subways and snow/ I’ve been thinking of places that we’ve
								never been/ And things we could do that you would enjoy/ And I’d
								hold you/ And you’d hold me/ I’ve been thinking of places that
								we’ve never been/ And things we could do and how I could kiss
								you/ And hold you/ And you’d hold me/ We can go wherever we
								want/ But you say let’s stay here/ Baby just lay here/ We’ll
								stay here/ My love
							</p>
						</div>
					</div>
				</div>
				<div class="row  hide-for-large-up " style="margin-right: 0px">
					<div class="small-12 medium-12 columns hide-for-large-up ">
						<ul class="small-block-grid-1 medium-block-grid-2"
							id="small-Lyrics">
							<li>
								<p>
									<strong>BACK //</strong>It’s been a while since I last saw you/
									21 days to be exact/ Though the road is where I lay my head/All
									of the streetlights say come back/ To you my love/ Oh my love/
									How many hours have I been driving/ So many places I’ve lost
									track/ Though the road is where my eyes look to/ My minds
									imagining me back/ With you my love/ Oh my love/ I’m coming
									home (I’m on my way)/ I’m on my way back home/ How many hours
									have I been driving/ So many places I’ve lost track/ But you
									don’t have to hold your head my dear/ Cause very soon I will be
									back/ To you my love/ Oh my love
								</p>
							</li>
							<li>
								<p>
									<strong>COMING HOME //</strong>The road goes on for days/ The
									clouds cry their tears of gray/ It’s getting colder the days
									getting dark/ But this stormy weather can’t keep me apart from
									you/ The music on the stereo sings “You’ve got so far to go”/
									I’m on my way/ The wind whispers I’m getting close signs with
									city names I know/ I’m on my way/ The road goes on for miles/
									And I’ve been thinking ’bout you for quite some time now/ It’s
									getting colder the days getting dark/ But this stormy weather
									can’t keep me apart from you/ The music on the stereo now sings
									“I wish I was homeward bound”/ I’m on my way/ The wind whispers
									I’m getting close familiar buildings, shops and roads/ I’m on
									my way/ I’m coming home
								</p>
							</li>
							<li>
								<p>
									<strong>SAND //</strong>I tried/ I tried my best to hold on/
									Hold on tight to you/ All in vain/ You slipped through my hands
									like a fistful of sand/ Drifting with the tides of the sea/ I’m
									trying to find some of my mind/ By fixing my faults from the
									past/ But it’s all in my head and the love that I bled/ Has
									shriveled and died in the cold/ You slipped through my hands
									like a fistful of sand/ Drifting with the tides of the sea/ You
									were in my hands…
								</p>
							</li>
							<li>
								<p>
									<strong>WAITING //</strong>Waiting for someone/ Waiting for
									something to be done/ I’ve been waiting for a while now/ For
									that good love to come/ Watching the sunrise/ Watching the
									clouds to blue to gray/ I’ve been waiting for the wind to blow/
									Good love my way/ What good’s a heart/ What good’s a heart with
									no one to love?/ Will I be waiting for forever/ I’ll find out
									soon enough/ Always analyzing/ What and when did I do wrong/
									Cause I held love in my hands once/ And in a second it was
									gone/ What good’s a heart/ What good’s a heart with no one to
									love/ Will I be waiting for forever darling/ We’ll find out
									soon enough/ Maybe love will take a while/ Maybe love might
									find me soon/ Maybe waiting slows me down/ And I can change my
									blues to our maroons/ I’ll find my way/ I’ll find your way/
									Watching the sunrise/ Watching the clouds turn blue and gray/
									You’ve been waiting for too long but don’t worry/ I’m on my way
								</p>
							</li>
							<li>
								<p>
									<strong>WE CAN GO WHEREVER WE WANT //</strong>We can go
									wherever we want/ Let’s go to the city/ Wander museums/ and
									stare at Picassos/ We can go wherever we want/ Let’s go to the
									country/ And hike in the valleys/ And I’ll bring the wine/ I’ve
									been thinking of places that we’ve never been/ And things we
									could do that you would enjoy/ And I’d hold you/ And you’d hold
									me/ We can go wherever we want/ Let’s drive to Los Angeles/
									Hold hands in the car/ Find out who we are/ Let’s buy things we
									don’t need/ We can go wherever we want/ Let’s fly to Boston/
									Let’s get lost in the subways and snow/ I’ve been thinking of
									places that we’ve never been/ And things we could do that you
									would enjoy/ And I’d hold you/ And you’d hold me/ I’ve been
									thinking of places that we’ve never been/ And things we could
									do and how I could kiss you/ And hold you/ And you’d hold me/
									We can go wherever we want/ But you say let’s stay here/ Baby
									just lay here/ We’ll stay here/ My love
								</p>
							</li>
							<li>
								<p>
									<strong>COUNTING DOWN THE DAYS //</strong>The thought of you/
									Just the thought of you/ Now the winter’s not so cold/ I’m
									counting down the days/ / ’til my love comes home/ When I hear
									your voice/ I feel your voice/ Vibrating through my soul/ I’m
									counting down the days/ ’til my love comes home
								</p>
							</li>
							<li>
								<p>
									<strong>FOREVER MEANS FOREVER //</strong>I love you so much/
									I’ll love you forever/ I mean forever/ I love you so much/ I’ll
									kiss you forever/ I’ll hold you forever/ When our hearts grow
									tired/ I’ll be sure to sing you this song/ When we lose
									control/ I’ll give in/ I’ll be wrong/ Cause forever means
									forever/ I love you so much/ I’ll be yours forever/ I am yours
									forever/ When our hearts grow tired/ I’ll be sure to sing you
									this song/ When we lose control/ I’ll give in/ I’ll be wrong/
									Cause forever means forever/ Forever means forever
								</p>
							</li>
							<li>
								<p>
									<strong>HESITATION //</strong>The thought of you/ Just the
									thought of you/ Now the winter’s not so cold/ I’m counting down
									the days/ ’til my love comes home/ When I hear your voice/ I
									feel your voice/ Vibrating through my soul/ I’m counting down
									the days/ ’til my love comes home/ And I’ll sing Ooh come home/
									What does this hesitation mean/ If I try harder I’ll make
									scene/ But when the prize is worth the risk/ I’ll take a chance
									and go for it/ What are her eyes telling me/ If I look harder I
									might blink/ But when her lips are worth my wait/ I can’t I
									won’t hesitate/ She’s so beautifully dressed/ My stomach’s in a
									mess/ Can I pull this off/ Pull of these butterflies/ What does
									this conversation mean/ If I keep quiet she might think/ I’m
									just here to waste her time/ But I’m just lost in her eyes/
									She’s so beautifully dressed/ My stomach’s in a mess/ Can I
									pull this off/ Pull of these butterflies/ My hands in my
									pockets/ My eyes on the table/ She wants me she wants me/ I
									know I am capable/ Eyes switch to dress/ Down to shoes down to
									floor/ She wants me she wants me/ But I want her more/ What
									does this conversation mean/ If I keep quiet she might think/
									That I’m just here to waste her time/ But I’m just lost in her
									eyes
								</p>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</section>
		<section id="videos" class="main-sections">
			<div class="section">
				<div class="row">
					<div class="large-12 medium-12 small-12 columns title left">
						<h1>videos</h1>
					</div>
				</div>
				<div class="row">
					<div class="large-12 medium-12 small-12 columns">
						<div id="video-wrapper">
							<iframe width="100%" height="100%" src="${video.link}"
								frameborder="0" allowfullscreen></iframe>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section id="events" class="main-sections">
			<div class="section">
				<div class="row">
					<div class="large-12 medium-12 small-12 columns title right">
						<h1>Shows</h1>
					</div>
				</div>
				<!--<div class="row">
                        <div class="large-12 medium-12 small-12 columns right">
                           <div style="width:45%; text-align:right"> 
                           <div class="show-slider">
                              <div class="slide-Container">
                                <img src="img/show.jpg">
                              </div>
                              <div class="slide-Container">
                                <img src="img/show.1.jpg">
                              </div>
                              <div class="slide-Container">
                                <img src="img/show.2.jpg">
                              </div>
                            </div>
                            </div>
                        </div>
                    </div>-->
				<div class="row">
					<div class="large-12 medium-12 small-12 columns right"
						style="text-align: right;">
						<h3>next stage</h3>
						<ul id="show-listings">
							<li class="show modal"><a>Lorem ipsum dolor sit amet,
									consectetur adipitetur ameitetura dolor sit</a></li>
							<li class="show modal"><a>Lorem ipsum dolor sit amet,
									consectetur adipitetur ameitetura dolor sit</a></li>
							<li class="show modal"><a>Lorem ipsum dolor sit amet,
									consectetur adipitetur ameitetura dolor sit</a></li>
						</ul>
					</div>
				</div>
			</div>
		</section>
		<section id="contact" class="main-sections">
			<div>
				<div class="row">
					<div class="large-12 medium-12 small-12 columns title left">
						<h1>contact</h1>
					</div>
				</div>
				<div class="row">
					<div class="large-6 medium-12 small-12 columns">
						<form id="contact-form">
							<h3>
								<span><a>newsletter </a>| </span> <span><a>Book Me</a>| </span>
								<span><a>questions</a></span>
							</h3>
							<input class="input-form" type="text" name="name"
								placeholder="name"> <br> <input class="input-form"
								type="text" name="email" placeholder="email"> <br>
							<textarea class="input-form" placeholder="message"></textarea>
							<br>
							<button type="submit" class="send right" value="SEND">
								<img class="navIconz" data-alt-src="img/send.4.png"
									src="img/send.3.png">
							</button>
						</form>
					</div>
					<div class="large-6 medium-12 small-12 columns"></div>
				</div>
			</div>
		</section>
	</div>
	<!-- modal -->
	<div id="eventModal" class="reveal-modal small" data-reveal>
		<div>
			<div class="row outer-title">
				<div class="large-12 medium-12 small-12 columns">more details
					of show here, option for adding to calader with google API.</div>
			</div>
			<a class="close-reveal-modal">X</a>
		</div>


		<script src="<c:url value="/js/jquery.js"/>"></script>
		<script src="<c:url value="/js/slick.min.js"/>"></script>
		<script src="<c:url value="/js/foundation.js"/>"></script>
		<script src="<c:url value="/js/foundation.reveal.js"/>"></script>
		<script src="<c:url value="/js/foundation.equalizer.js"/>"></script>

		<script>
			$(document).foundation();

			//smooth scrolling 
			$(function() {
				$(document).on("scroll", onScroll);
				$('a[href*=#]:not([href=#])')
						.click(
								function() {
									if (location.pathname.replace(/^\//, '') == this.pathname
											.replace(/^\//, '')
											&& location.hostname == this.hostname) {
										var target = $(this.hash);
										console.log(target);
										target = target.length ? target
												: $('[name='
														+ this.hash.slice(1)
														+ ']');
										if (target.length) {
											$('html,body').animate({
												scrollTop : target.offset().top
											}, 1000);
											return false;
										}
									}
								});
			});

			function onScroll(event) {
				var scrollPos = $(document).scrollTop();
				var homePos = $("#home").height();
				if (scrollPos > homePos) {
					$(".sidescroll").removeClass('hide');
					//$( ".sidescroll" ).animate({ "right": "50px" }, "slow" );
				} else {
					$(".sidescroll").addClass('hide');
					//$( ".sidescroll" ).animate({ "left": "50px" }, "slow" );
				}
			}

			//modal 
			$('.modal').click(function() {
				$('#eventModal').foundation('reveal', 'open');
			});

			//top nav img hover
			var sourceSwap = function() {
				var $this = $(this);
				var newSource = $this.data('alt-src');
				$this.data('alt-src', $this.attr('src'));
				$this.attr('src', newSource);
			}
			$(function() {
				$('img.navIconz').hover(sourceSwap, sourceSwap);
			});

			//show icon overlay 

			$('#showIcon a').hover(function() {
				$(this).removeClass('showOverlay');
			});

			//slider
			$('.show-slider').slick({
				slidesToShow : 1,
				slidesToScroll : 1,
				autoplay : true,
				autoplaySpeed : 3000,

			});

			//audio player
			$(document).ready(function() {
				$("#click-listen").click(function() {
					$("#player-container").toggleClass("hide");
				});
			});

			$('.musicNote').click(function() {
				$('.words').toggleClass("hide");

			});

			$(document).on('keydown', function(e) {
				if (e.keyCode == 82) {
					console.log('r key = rewind');
					rewindAudio();
				} else if (e.keyCode == 70) {
					console.log('f key = forward');
					forwardAudio();
				}
			});

			function rewindAudio() {
				console.log('rewindAudio');
				oAudio = $('audio')[0];
				oAudio.currentTime -= 1.0;
			}
			function forwardAudio() {
				console.log('forwardAudio');
				oAudio = $('audio')[0];
				oAudio.currentTime += 1.0;
			}
		</script>
</body>
</html>