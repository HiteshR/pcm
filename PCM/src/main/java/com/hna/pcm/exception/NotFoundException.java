package com.hna.pcm.exception;

public class NotFoundException extends BaseWebApplicationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4553101964302123808L;

	public NotFoundException(NotFound notFound) {
		super(404, notFound.getStatusCode(), notFound.getErrorMessage(),
				notFound.getDeveloperMessage());
	}

	public enum NotFound {
		UserNotFound("40401", "User Not Found",	"No User could be found for that Id"),
		AudioNotFound("","File not found", "Audio not found in database"),
		ImageNotFound("","File not found", "Image not found in database"),
		ShowFound("","Show not found", "Show not found in database"),
		TokenNotFound("40403", "Token Not Found","No token could be found for that Id");

		private String statusCode;
		private String errorMessage;
		private String developerMessage;

		private NotFound(String statusCode, String errorMessage,
				String developerMessage) {
			this.statusCode = statusCode;
			this.errorMessage = errorMessage;
			this.developerMessage = developerMessage;
		}

		public String getStatusCode() {
			return statusCode;
		}

		public String getErrorMessage() {
			return errorMessage;
		}

		public String getDeveloperMessage() {
			return developerMessage;
		}

	}
}
