package com.hna.pcm.exception;

public class FileExistException extends BaseWebApplicationException {

	public FileExistException(String errorMessage) {
		super(409, "40901", errorMessage, "File already exist in database");
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
