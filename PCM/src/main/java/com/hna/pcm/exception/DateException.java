package com.hna.pcm.exception;

public class DateException extends BaseWebApplicationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DateException(DateExceptionErroCode dateExceptionErroCode) {
		super(409, dateExceptionErroCode.getStatusCode(), dateExceptionErroCode
				.getErrorMessage(), dateExceptionErroCode.getDeveloperMessage());
	}

	public enum DateExceptionErroCode {
		EndDateGreaterException("409014",
				"Enddate must be greater than start date",
				"Enddate must be greater than start date"), ParsingNotSupportedException(
				"409014", "Date parsing not supported",
				"Date parsing not supported"), TextToParseIsInvalidException(
				"409014", "Enddate must be greater than start date",
				"Enddate must be greater than start date"), PatternIsInvalidException(
				"409014", "Not valid date patter", "Not valid date patter");

		private String statusCode;
		private String errorMessage;
		private String developerMessage;

		private DateExceptionErroCode(String statusCode, String errorMessage,
				String developerMessage) {
			this.statusCode = statusCode;
			this.errorMessage = errorMessage;
			this.developerMessage = developerMessage;
		}

		public String getStatusCode() {
			return statusCode;
		}

		public String getErrorMessage() {
			return errorMessage;
		}

		public String getDeveloperMessage() {
			return developerMessage;
		}
	}
}
