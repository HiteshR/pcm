package com.hna.pcm.exception;

public class ExtentionException extends BaseWebApplicationException {

	public ExtentionException(String errorMessage) {
		super(404, "404", errorMessage, "File extention not matched");
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
