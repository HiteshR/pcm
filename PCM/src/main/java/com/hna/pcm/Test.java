package com.hna.pcm;

import java.util.Date;
import java.util.TimeZone;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class Test {
	public static void main(String[] args) {
		// BCryptPasswordEncoder p = new BCryptPasswordEncoder();
		// System.out.println(p.encode("hr"));
		// DateTimeZone timeZoneKolkata = DateTimeZone.forID("America");
		//
		// long millis = 1389975349000L;
		// DateTime dateTimeUtc = new DateTime(DateTimeZone.UTC);
		// DateTime dateTimeKolkata = dateTimeUtc.toDateTime(timeZoneKolkata);
		// System.out.println("dateTimeUtc: " + dateTimeUtc);
		// System.out.println("dateTimeKolkata: " + dateTimeKolkata);
		// TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
		// DateTimeFormatter objDateFormat = DateTimeFormat
		// .forPattern("MM/dd/yyyy HH:mm");
		// LocalDateTime objLocalDateTime = objDateFormat.parseLocalDateTime(
		// "01/27/2015 09:30");
		// objLocalDateTime.now(DateTimeZone.forID("Europe/Paris"));
		// System.out.println(objLocalDateTime);

//		DateTimeFormatter df = DateTimeFormat
//				.forPattern("MM/dd/yyyy HH:mm:ss Z ");
//		DateTime dateTime = df.withOffsetParsed().parseDateTime(
//				"02/22/2015 18:19:00 +1000");
//		System.out.println(dateTime.toDate());
		
		//System.out.println(TimeZone.getDefault());
		//TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
		//System.out.println(TimeZone.getTimeZone("Asia/Calcutta").getRawOffset());
		
		
//		DateTimeFormatter df = DateTimeFormat.forPattern("MM/dd/yyyy HH:mm 'UTC'Z");
//	    DateTime temp = df.withOffsetParsed().parseDateTime("11/30/2012 12:08 UTC+05:30");
//	    
//	    Date date = temp.toDate();
//	    System.out.println(date);
		
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
		DateTimeFormatter df = DateTimeFormat.forPattern("MM/dd/yyyy HH:mm 'UTC'Z");
	    DateTime temp = df.withOffsetParsed().parseDateTime("11/30/2012 08:00 UTC-08:00");
	    
	    Date date = temp.toDate();
	    System.out.println(date);
	}
}
