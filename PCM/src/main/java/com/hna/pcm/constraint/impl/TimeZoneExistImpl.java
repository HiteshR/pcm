package com.hna.pcm.constraint.impl;

import javax.annotation.Resource;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.util.StringUtils;

import com.hna.pcm.constraint.TimeZoneExist;
import com.hna.pcm.repository.TimeZoneRepository;

public class TimeZoneExistImpl implements
		ConstraintValidator<TimeZoneExist, String> {
	@Resource
	private TimeZoneRepository timeZoneRepository;

	private String emptyString;
	private boolean isRequire;

	@Override
	public void initialize(TimeZoneExist constraintAnnotation) {
		emptyString = constraintAnnotation.emptyString();
		isRequire = constraintAnnotation.isRequire();
	}

	@Override
	public boolean isValid(String zoneName, ConstraintValidatorContext context) {
		if (isRequire && !StringUtils.hasText(zoneName)) {
			context.disableDefaultConstraintViolation();
			context.buildConstraintViolationWithTemplate(emptyString)
					.addConstraintViolation();
			return false;
		} else if (StringUtils.hasText(zoneName)) {
			
			return true;
		} else if (!isRequire) {
			return true;
		}
		return false;
	}
}
