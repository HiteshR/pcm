package com.hna.pcm.constraint.impl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.util.StringUtils;

import com.hna.pcm.constraint.PasswordMatch;

public class PasswordMatchImpl implements
		ConstraintValidator<PasswordMatch, Object> {

	// private String password;
	private String emptyPasswordMessage;
	private String passwordProperty;
	private int min;
	private int max;
	private String passwordLengthMessage;

	// private String confirmPassword;
	private String confirmPasswordProperty;
	private String emptyConfirmPassword;

	@Override
	public void initialize(PasswordMatch pm) {
		// password = pm.password();
		emptyPasswordMessage = pm.emptyPasswordMessage();
		passwordProperty = pm.passwordproperty();
		min = pm.min();
		max = pm.max();
		passwordLengthMessage = pm.passwordLengthMessage();

		// confirmPassword = pm.confirmPassword();
		confirmPasswordProperty = pm.confirmPasswordproperty();
		emptyConfirmPassword = pm.emptyConfirmPasswordMessage();
	}

	@Override
	public boolean isValid(Object obj, ConstraintValidatorContext context) {

		try {
			boolean isValid = true;
			// get field value
			String password = BeanUtils.getProperty(obj, this.passwordProperty);
			String repassword = BeanUtils.getProperty(obj,
					this.confirmPasswordProperty);

			if (!StringUtils.hasText(password)) {
				context.disableDefaultConstraintViolation();
				context.buildConstraintViolationWithTemplate(
						emptyPasswordMessage).addPropertyNode(passwordProperty)
						.addConstraintViolation();
				isValid = false;
			} else if (!(password.length() >= min)
					|| !(password.length() <= max)) {
				context.disableDefaultConstraintViolation();
				context.buildConstraintViolationWithTemplate(
						passwordLengthMessage)
						.addPropertyNode(passwordProperty)
						.addConstraintViolation();
				isValid = false;
			}

			if (!StringUtils.hasText(repassword)) {
				context.disableDefaultConstraintViolation();
				context.buildConstraintViolationWithTemplate(
						emptyConfirmPassword)
						.addPropertyNode(confirmPasswordProperty)
						.addConstraintViolation();
				isValid = false;
			}

			if (isValid) {
				if (!password.equals(repassword)) {
					context.disableDefaultConstraintViolation();
					context.buildConstraintViolationWithTemplate(
							context.getDefaultConstraintMessageTemplate())
							.addPropertyNode(confirmPasswordProperty)
							.addConstraintViolation();
					isValid = false;
				}
			}

			return isValid;
		} catch (Exception ex) {
			return false;
		}
	}
}
