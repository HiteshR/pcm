package com.hna.pcm.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "pcm_image")
public class Image extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name = "name", length = 50, unique = true, nullable = false)
	private String name;

	// @Column(name = "description", length = 250)
	// private String description;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
