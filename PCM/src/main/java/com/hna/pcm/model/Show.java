package com.hna.pcm.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "pcm_show")
public class Show extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name = "city", length = 100, nullable = false)
	private String city;

	@Column(name = "venue", length = 100, nullable = false)
	private String venue;

	@Column(name = "detail", length = 250)
	private String detail;

	@Column(name = "show_date", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date showDate;

	@ManyToOne(targetEntity = TimeZone.class, fetch = FetchType.LAZY)
	@JoinColumn(name = "time_zone", nullable = false)
	private TimeZone timeZone;

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getVenue() {
		return venue;
	}

	public void setVenue(String venue) {
		this.venue = venue;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public Date getShowDate() {
		return showDate;
	}

	public void setShowDate(Date showDate) {
		this.showDate = showDate;
	}

	public TimeZone getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(TimeZone timeZone) {
		this.timeZone = timeZone;
	}

}
