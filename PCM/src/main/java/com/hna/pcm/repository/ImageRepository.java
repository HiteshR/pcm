package com.hna.pcm.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import com.hna.pcm.dto.ImageDto;
import com.hna.pcm.model.Image;

public interface ImageRepository extends BaseRepository<Image> {

	@Query(value = "SELECT new com.hna.pcm.dto.ImageDto(image.name,image.id) FROM Image AS image")
	public List<ImageDto> getAllImages();
}
