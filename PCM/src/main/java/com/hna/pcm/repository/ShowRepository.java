package com.hna.pcm.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.hna.pcm.dto.ShowDisplayDto;
import com.hna.pcm.dto.ShowDto;
import com.hna.pcm.model.Show;

public interface ShowRepository extends BaseRepository<Show> {

	@Query(value = "SELECT new com.hna.pcm.dto.ShowDisplayDto(show.id,show.city,show.venue,show.detail,show.showDate) FROM Show AS show")
	public List<ShowDisplayDto> getAllShow();

	// @Query(value =
	// "SELECT new com.hna.pcm.dto.ShowDto(show.city,show.venue,show.detail,show.showDate) FROM Show AS show WHERE show.showDate>=NOW()")
	@Query(value = "SELECT new com.hna.pcm.dto.ShowDisplayDto(show.id,show.city,show.venue,show.detail,DATE(CONVERT_TZ(show.showDate,'+00:00',timeZone.zone))) FROM Show AS show JOIN show.timeZone AS timeZone WHERE CONVERT_TZ(show.showDate,'+00:00',timeZone.zone)>CONVERT_TZ(UTC_TIMESTAMP(),'+00:00',timeZone.zone)")
	public List<ShowDisplayDto> getAllActiveShow();

	@Query(value = "SELECT new com.hna.pcm.dto.ShowDto(show.city,show.venue,show.detail,DATE_FORMAT(CONVERT_TZ(show.showDate,'+00:00',timeZone.zone),'%m/%d/%Y %H:%i'),timeZone.name) FROM Show AS show JOIN show.timeZone AS timeZone WHERE show.id=:id")
	public ShowDto getShow(@Param("id") Long id);
}
