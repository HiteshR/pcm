package com.hna.pcm.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.hna.pcm.model.TimeZone;

public interface TimeZoneRepository extends BaseRepository<TimeZone> {

	@Query(value = "SELECT timeZone.name FROM TimeZone AS timeZone")
	public List<String> getAllTimeZoneName();

	@Query(value = "SELECT timeZone FROM TimeZone AS timeZone WHERE timeZone.name=:name")
	public TimeZone getTimeZoneByName(@Param("name") String name);

}
