package com.hna.pcm.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.hna.pcm.model.User;

public interface UserRepository extends BaseRepository<User> {

	//@Query(value = "SELECT user.email FROM User AS user WHERE user.email=:email")
	//public boolean isEmailPresent(@Param("email") String email);

	@Query(value = "SELECT user FROM User AS user WHERE user.email=:email")
	public User getUserByEmail(@Param("email") String email);

}
