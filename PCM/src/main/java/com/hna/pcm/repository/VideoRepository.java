package com.hna.pcm.repository;

import org.springframework.data.jpa.repository.Query;

import com.hna.pcm.dto.VideoDto;
import com.hna.pcm.model.Video;

public interface VideoRepository extends BaseRepository<Video> {

	// @Query(value =
	// "SELECT about.content FROM About AS about WHERE about.active=true")
	// public String getContent();

	// @Query(value =
	// "SELECT com.hna.pcm.dto.VideoDto(video.link) FROM Video AS video WHERE video.active=true")
	// public VideoDto getVideoLink();

	@Query(value = "SELECT new com.hna.pcm.dto.VideoDto(video.link) FROM Video AS video WHERE video.active=true")
	public VideoDto getVideoLink();

	@Query(value = "SELECT video FROM Video AS video WHERE video.active=true")
	public Video getVideo();
}
