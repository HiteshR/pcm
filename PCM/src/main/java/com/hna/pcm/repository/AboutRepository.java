package com.hna.pcm.repository;

import org.springframework.data.jpa.repository.Query;

import com.hna.pcm.model.About;

public interface AboutRepository extends BaseRepository<About> {

	//@Query(value = "SELECT about.content FROM About AS about WHERE about.active=true")
	//public String getContent();
	
	@Query(value = "SELECT about FROM About AS about WHERE about.active=true")
	public About getContent();
}
