package com.hna.pcm.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import com.hna.pcm.dto.AudioDto;
import com.hna.pcm.model.Audio;

public interface AudioRepository extends BaseRepository<Audio> {

	@Query(value = "SELECT new com.hna.pcm.dto.AudioDto(audio.trackName,audio.fileName,audio.id) FROM Audio AS audio")
	public List<AudioDto> getAllAudios();
}
