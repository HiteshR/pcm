package com.hna.pcm.dto;

import com.hna.pcm.constraint.EmailNotExist;

public class ForgotPasswordDto {

	@EmailNotExist(emailEmptyError = "Please enter email address", emailNotExistError = "Email address not found.", invalidEmailError = "Not valid email adderess")
	private String email;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
