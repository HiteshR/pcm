package com.hna.pcm.dto;

import org.hibernate.validator.constraints.NotEmpty;

import com.hna.pcm.constraint.PasswordMatch;

@PasswordMatch(passwordproperty = "password", confirmPasswordproperty = "confirmPassword", message = "Password Not Match", emptyPasswordMessage = "password must not empty", emptyConfirmPasswordMessage = "ConfirmPassword password must not empty", max = 40, min = 8, passwordLengthMessage = "Password must be 8 to 40 char long")
public class ResetPasswordDTO {

	@NotEmpty(message = "token must not be empty")
	private String token;

	private String password;

	private String confirmPassword;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

}
