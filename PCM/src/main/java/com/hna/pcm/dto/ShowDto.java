package com.hna.pcm.dto;

import org.hibernate.validator.constraints.NotEmpty;

import com.hna.pcm.constraint.DateFormat;

public class ShowDto {

	@NotEmpty(message = "Please enter city")
	private String city;

	@NotEmpty(message = "Please enter venue")
	private String venue;

	private String detail;

	@DateFormat(isRequire = true, emptyString = "Please select date", pattern = "MM/dd/yyyy HH:mm", wrongPattern = "valid pattern is MM/dd/yyyy HH:mm")
	private String date;

	@NotEmpty(message = "Please select timezone")
	private String timeZone;

	public ShowDto(String city, String venue, String detail, String date,
			String timeZone) {
		super();
		this.city = city;
		this.venue = venue;
		this.detail = detail;
		this.date = date;
		this.timeZone = timeZone;
	}

	public ShowDto() {

	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getVenue() {
		return venue;
	}

	public void setVenue(String venue) {
		this.venue = venue;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

}
