package com.hna.pcm.dto;

import org.springframework.web.multipart.MultipartFile;

public class AudioFileDto {

	private MultipartFile attachment;
	private String name;

	public MultipartFile getAttachment() {
		return attachment;
	}

	public void setAttachment(MultipartFile attachment) {
		this.attachment = attachment;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
