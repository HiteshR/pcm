package com.hna.pcm.dto;

public class AudioDto {

	private String trackName;
	private String fileName;
	private Long id;

	public AudioDto() {

	}

	public AudioDto(String trackName, String fileName, Long id) {
		super();
		this.trackName = trackName;
		this.fileName = fileName;
		this.id = id;
	}

	public String getTrackName() {
		return trackName;
	}

	public void setTrackName(String trackName) {
		this.trackName = trackName;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
