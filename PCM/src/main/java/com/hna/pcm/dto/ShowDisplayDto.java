package com.hna.pcm.dto;

import java.util.Date;

public class ShowDisplayDto {

	private Long id;
	private String city;

	private String venue;

	private String detail;

	private Date date;

	public ShowDisplayDto(Long id, String city, String venue, String detail,
			Date date) {
		super();
		this.id = id;
		this.city = city;
		this.venue = venue;
		this.detail = detail;
		this.date = date;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getVenue() {
		return venue;
	}

	public void setVenue(String venue) {
		this.venue = venue;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

}
