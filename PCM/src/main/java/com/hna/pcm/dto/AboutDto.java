package com.hna.pcm.dto;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author Administrator
 *
 */
public class AboutDto {

	@NotEmpty(message="Please add content")
	private String content;

	public AboutDto() {

	}

	public AboutDto(String content) {
		this.content = content;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}
