package com.hna.pcm.dto;

import org.hibernate.validator.constraints.NotEmpty;

import com.hna.pcm.constraint.PasswordMatch;

@PasswordMatch(passwordproperty = "newPassword", confirmPasswordproperty = "confirmPassword", message = "Password Not Match", emptyPasswordMessage = "password must not empty", emptyConfirmPasswordMessage = "confirm password must not empty", max = 40, min = 8, passwordLengthMessage = "Password must be 8 to 40 char long")
public class ChangePasswordDto {

	private String newPassword;

	private String confirmPassword;

	@NotEmpty(message = "Please enter current password")
	private String currentPassword;

	public String getCurrentPassword() {
		return currentPassword;
	}

	public void setCurrentPassword(String currentPassword) {
		this.currentPassword = currentPassword;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

}
