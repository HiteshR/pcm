package com.hna.pcm.dto;

import org.hibernate.validator.constraints.NotEmpty;

import com.hna.pcm.constraint.EmailNotExist;

/**
 * @author Administrator
 *
 */
public class LoginDto {

	@EmailNotExist(emailEmptyError = "Please enter email address", emailNotExistError = "Email address not found.", invalidEmailError = "Not valid email adderess")
	private String email;

	@NotEmpty(message = "Please enter password")
	private String password;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
