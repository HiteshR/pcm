package com.hna.pcm.dto;

public class ImageDto {

	private String name;
	private Long id;

	public ImageDto() {

	}

	public ImageDto(String name, Long id) {
		super();
		this.name = name;
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
