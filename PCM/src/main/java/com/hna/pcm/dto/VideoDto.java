package com.hna.pcm.dto;

import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.URL;

public class VideoDto {

	@NotEmpty(message = "Url require")
	@URL(message = "Not valid url")
	private String link;

	public VideoDto() {

	}

	public VideoDto(String link) {
		super();
		this.link = link;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

}
