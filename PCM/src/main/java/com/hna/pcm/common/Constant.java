package com.hna.pcm.common;

public interface Constant {

	String SESSION_USER = "sessionUser";

	String DATE_FORMAT = "MM/dd/yyyy HH:mm";
	String DATE_FORMAT_WITH_TIME = "dd/MM/yyyy HH:mm:ss.SSS";

	String ERR_MSG = "errorMSG";
	String SUCCESS_MSG = "successMSG";

	// TODO jsp
	String VIEW_LOGIN = "user/login";
	String VIEW_HOME = "admin-home";
	String VIEW_ABOUT = "about";
	String VIEW_VIDEO = "video";
	String VIEW_IMAGE_LIST = "image-list";
	String VIEW_AUDIO_LIST = "audio-list";
	String VIEW_SHOW_LIST = "show-list";
	String VIEW_ACCOUNT = "account";

	String ERROR_LOGIN_MESSAGE = "Please Login...";

	Integer REGISTER_EMAIL_LINK_EXPIRE_TIME = 12 * 24;
	Integer LOST_PASSWORD_EMAIL_LINK_EXPIRE_TIME = 12 * 24;

	String EMAIL_HEADER_RESET_PASSWORD = "Reset Password";

	String TEMPLATE_EMAIL_RESET_PASSWORD = "resetPassword.ftl";
}
