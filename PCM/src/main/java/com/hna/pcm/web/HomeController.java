package com.hna.pcm.web;

import java.io.IOException;
import java.util.List;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.hna.pcm.common.Constant;
import com.hna.pcm.dto.AboutDto;
import com.hna.pcm.dto.AudioDto;
import com.hna.pcm.dto.ChangePasswordDto;
import com.hna.pcm.dto.ForgotPasswordDto;
import com.hna.pcm.dto.ImageDto;
import com.hna.pcm.dto.LoginDto;
import com.hna.pcm.dto.ResetPasswordDTO;
import com.hna.pcm.dto.ResponseMessageDto;
import com.hna.pcm.dto.ShowDisplayDto;
import com.hna.pcm.dto.ShowDto;
import com.hna.pcm.dto.ValidationErrorDTO;
import com.hna.pcm.dto.VideoDto;
import com.hna.pcm.exception.AuthorizationException;
import com.hna.pcm.exception.BaseWebApplicationException;
import com.hna.pcm.exception.FieldErrorException;
import com.hna.pcm.exception.FileExistException;
import com.hna.pcm.exception.MessageException;
import com.hna.pcm.exception.MessageException.MessageExceptionErroCode;
import com.hna.pcm.model.About;
import com.hna.pcm.model.TimeZone;
import com.hna.pcm.model.User;
import com.hna.pcm.service.AboutService;
import com.hna.pcm.service.AudioService;
import com.hna.pcm.service.ImageService;
import com.hna.pcm.service.ShowService;
import com.hna.pcm.service.TimeZoneService;
import com.hna.pcm.service.UserService;
import com.hna.pcm.service.VideoService;

import freemarker.template.TemplateException;

@Controller
public class HomeController {

	@Autowired
	private UserService userService;

	@Autowired
	private AboutService aboutService;

	@Autowired
	private AudioService audioService;

	@Autowired
	private ImageService imageService;

	@Autowired
	private ShowService showService;

	@Autowired
	private TimeZoneService timeZoneService;

	@Autowired
	private VideoService videoService;

	@RequestMapping(value = "home", method = RequestMethod.GET)
	public String home1(HttpSession session, ModelMap modelMap) {
		modelMap.put("about", this.aboutService.getContent());
		modelMap.put("video", this.videoService.getVideoLink());
		modelMap.put("shows", this.showService.getAllActiveShow());
		modelMap.put("audios", this.audioService.getAudios());
		return "home";
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(HttpSession session) {
		User user = (User) session.getAttribute(Constant.SESSION_USER);
		if (user != null) {
			return "redirect:about";
		} else {
			return "redirect:login";
		}
	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logOut(HttpSession session) {
		session.invalidate();
		return "redirect:login";
	}

	// TODO login
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String loginPre(HttpSession session, ModelMap modelMap) {
		User user = (User) session.getAttribute(Constant.SESSION_USER);
		if (user != null) {
			return "redirect:about";
		} else {
			if (!modelMap.containsAttribute("loginUser"))
				modelMap.put("loginUser", new LoginDto());
			return Constant.VIEW_LOGIN;
		}
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String loginPost(
			@Valid @ModelAttribute("loginUser") LoginDto loginUser,
			BindingResult result, HttpSession session,
			RedirectAttributes redirectAttributes) {
		try {
			if (!result.hasErrors()) {
				User objUser = this.userService.login(loginUser);
				session.setAttribute(Constant.SESSION_USER, objUser);
				return "redirect:/";
			} else {
				redirectAttributes.addFlashAttribute("loginUser", loginUser);
				redirectAttributes.addFlashAttribute(
						BindingResult.MODEL_KEY_PREFIX + "loginUser", result);
				return "redirect:login";
			}
		} catch (BaseWebApplicationException exception) {
			redirectAttributes.addFlashAttribute("loginUser", loginUser);
			redirectAttributes.addFlashAttribute(Constant.ERR_MSG,
					exception.getErrorMessage());
			return "redirect:login";
		}
	}

	@RequestMapping(value = "/account", method = RequestMethod.GET)
	public String setting(HttpSession session, ModelMap modelMap) {
		User user = (User) session.getAttribute(Constant.SESSION_USER);
		if (user != null) {
			return Constant.VIEW_ACCOUNT;
		} else {
			return "redirect:login";
		}
	}

	//
	@RequestMapping(value = "/forgot-password", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseStatus(value = HttpStatus.OK)
	@ResponseBody
	public ResponseMessageDto forgotPassword(
			@RequestBody @Valid ForgotPasswordDto forgotPasswordModel,
			HttpServletRequest request) {
		try {
			StringBuilder objSB = new StringBuilder();
			objSB.append(request.getScheme()).append("://")
					.append(request.getServerName()).append(":")
					.append(request.getServerPort())
					.append(request.getContextPath()).append("/reset-password");
			this.userService.forgotPassword(forgotPasswordModel,
					objSB.toString());
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TemplateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new ResponseMessageDto(
				"Please check your mail for reset password");
	}

	@RequestMapping(value = "/reset-password", method = RequestMethod.GET)
	public String resetPasswordPre(@RequestParam("key") String token,
			ModelMap modelMap) {
		if (StringUtils.hasText(token)) {
			ResetPasswordDTO resetPasswordDTO = (ResetPasswordDTO) modelMap
					.get("resetPassword");
			if (resetPasswordDTO == null)
				resetPasswordDTO = new ResetPasswordDTO();
			resetPasswordDTO.setToken(token);
			modelMap.put("resetPassword", resetPasswordDTO);
			return "user/resetPassword";
		}
		return "redirect:/";
	}

	@RequestMapping(value = "/reset-password", method = RequestMethod.POST)
	public String resetPasswordPost(
			@ModelAttribute("resetPassword") @Validated ResetPasswordDTO resetPassword,
			BindingResult result, RedirectAttributes redirectAttributes) {
		try {
			if (StringUtils.hasText(resetPassword.getToken())) {
				if (result.hasErrors()) {
					redirectAttributes.addFlashAttribute(
							BindingResult.MODEL_KEY_PREFIX + "resetPassword",
							result);
					redirectAttributes.addFlashAttribute("resetPassword",
							resetPassword);
					return "redirect:/reset-password?key="
							+ resetPassword.getToken();
				} else {
					this.userService.resetPassword(resetPassword);
					return "redirect:/";
				}
			}
			return "redirect:/";
		} catch (BaseWebApplicationException baseWebApplicationException) {
			redirectAttributes.addFlashAttribute("errorMessage",
					baseWebApplicationException.getErrorMessage());
			redirectAttributes
					.addFlashAttribute("resetPassword", resetPassword);
			return "redirect:/user/reset-password?key="
					+ resetPassword.getToken();
		}
	}

	@RequestMapping(value = "/change-password", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseStatus(value = HttpStatus.OK)
	@ResponseBody
	public ResponseMessageDto changePassword(
			@RequestBody @Valid ChangePasswordDto changePasswordDto,
			HttpSession session) {
		User user = (User) session.getAttribute(Constant.SESSION_USER);
		if (user != null) {
			this.userService.changePassword(changePasswordDto, user);
			return new ResponseMessageDto("Password change Successfully");
		} else {
			throw new AuthorizationException(Constant.ERROR_LOGIN_MESSAGE);
		}
	}

	// TODO about
	@RequestMapping(value = "/about", method = RequestMethod.GET)
	public String aboutPre(HttpSession session, ModelMap modelMap) {
		User user = (User) session.getAttribute(Constant.SESSION_USER);
		if (user != null) {
			About about = this.aboutService.getContent();
			if (about == null)
				about = new About();
			modelMap.put("about", new AboutDto(about.getContent()));
			return Constant.VIEW_ABOUT;
		} else {
			return "redirect:login";
		}
	}

	@RequestMapping(value = "/about", method = RequestMethod.POST)
	public String aboutPost(@Valid @ModelAttribute("about") AboutDto about,
			BindingResult result, HttpSession session,
			RedirectAttributes redirectAttributes) {
		User user = (User) session.getAttribute(Constant.SESSION_USER);
		if (user != null) {
			if (!result.hasErrors()) {
				this.aboutService.saveContent(about.getContent(), user);
				redirectAttributes.addFlashAttribute(Constant.SUCCESS_MSG,
						"Updated successfully");
			} else {
				redirectAttributes.addFlashAttribute("about", about);
			}
			return "redirect:about";
		} else {
			return "redirect:login";
		}
	}

	// TODO video
	@RequestMapping(value = "/video", method = RequestMethod.GET)
	public String videoPre(HttpSession session, ModelMap modelMap) {
		User user = (User) session.getAttribute(Constant.SESSION_USER);
		if (user != null) {
			if (!modelMap.containsAttribute("video")) {
				VideoDto video = this.videoService.getVideoLink();
				if (video == null)
					video = new VideoDto();
				modelMap.put("video", video);
			}
			return Constant.VIEW_VIDEO;
		} else {
			return "redirect:login";
		}
	}

	@RequestMapping(value = "/video", method = RequestMethod.POST)
	public String videoPost(@Valid @ModelAttribute("video") VideoDto videoDto,
			BindingResult result, HttpSession session,
			RedirectAttributes redirectAttributes) {
		User user = (User) session.getAttribute(Constant.SESSION_USER);
		if (user != null) {
			if (!result.hasErrors()) {
				this.videoService.saveLink(videoDto, user);
			} else {
				redirectAttributes.addFlashAttribute("video", videoDto);
				redirectAttributes.addFlashAttribute(
						BindingResult.MODEL_KEY_PREFIX + "video", result);
			}
			return "redirect:video";
		} else {
			return "redirect:login";
		}
	}

	// TODO audio
	@RequestMapping(value = "/audio", method = RequestMethod.GET)
	public String audioPre(HttpSession session, ModelMap modelMap) {
		User user = (User) session.getAttribute(Constant.SESSION_USER);
		if (user != null) {
			modelMap.put("audios", this.audioService.getAudios());
			return Constant.VIEW_AUDIO_LIST;
		} else {
			return "redirect:login";
		}
	}

	@RequestMapping(value = "/get-audio-list", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody List<AudioDto> getAudio(HttpSession session,
			ModelMap modelMap) {
		User user = (User) session.getAttribute(Constant.SESSION_USER);
		if (user != null) {
			return this.audioService.getAudios();
		} else {
			throw new AuthorizationException(Constant.ERROR_LOGIN_MESSAGE);
		}
	}

	@RequestMapping(value = "/delete-audio/{id}", method = RequestMethod.DELETE, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody ResponseMessageDto deleteAudio(
			@PathVariable("id") Long id, HttpSession session) {
		User user = (User) session.getAttribute(Constant.SESSION_USER);
		if (user != null) {
			this.audioService.delete(id);
			return new ResponseMessageDto("Successfully");
		} else {
			throw new AuthorizationException(Constant.ERROR_LOGIN_MESSAGE);
		}
	}

	@RequestMapping(value = "/add-audio", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseStatus(value = HttpStatus.OK)
	@ResponseBody
	public ResponseMessageDto addAudio(
			// @RequestBody AudioFileDto audioFileDto,
			@RequestParam(value = "trackName", required = true) String trackName,
			@RequestParam(value = "attachment", required = true) final MultipartFile audio,
			HttpSession session, HttpServletRequest servletRequest) {
		User user = (User) session.getAttribute(Constant.SESSION_USER);
		if (user != null) {
			try {
				this.audioService.uploadAudio(user, audio, trackName,
						servletRequest);
				return new ResponseMessageDto("Upload successfully");
			} catch (IllegalStateException e) {
				throw new MessageException(MessageExceptionErroCode.FILEUPLOAD);
			} catch (IOException e) {
				throw new MessageException(MessageExceptionErroCode.FILEUPLOAD);
			} catch (DataIntegrityViolationException dataIntegrityViolationException) {
				throw new FileExistException("Same file already exist");
			}
		} else {
			throw new AuthorizationException(Constant.ERROR_LOGIN_MESSAGE);
		}
	}

	// TODO image
	@RequestMapping(value = "/image", method = RequestMethod.GET)
	public String imageList(HttpSession session, ModelMap modelMap) {
		User user = (User) session.getAttribute(Constant.SESSION_USER);
		if (user != null) {
			return Constant.VIEW_IMAGE_LIST;
		} else {
			return "redirect:login";
		}
	}

	@RequestMapping(value = "/get-image-list", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody List<ImageDto> getImages(HttpSession session,
			ModelMap modelMap) {
		User user = (User) session.getAttribute(Constant.SESSION_USER);
		if (user != null) {
			return this.imageService.getImages();
		} else {
			throw new AuthorizationException(Constant.ERROR_LOGIN_MESSAGE);
		}
	}

	@RequestMapping(value = "/delete-image/{id}", method = RequestMethod.DELETE, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody ResponseMessageDto deleteImage(
			@PathVariable("id") Long id, HttpSession session) {
		User user = (User) session.getAttribute(Constant.SESSION_USER);
		if (user != null) {
			this.imageService.delete(id);
			return new ResponseMessageDto("Successfully");
		} else {
			throw new AuthorizationException(Constant.ERROR_LOGIN_MESSAGE);
		}
	}

	@RequestMapping(value = "/add-image", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseStatus(value = HttpStatus.OK)
	@ResponseBody
	public ResponseMessageDto addImage(
			// @RequestBody AudioFileDto audioFileDto,
			@RequestParam(value = "attachment", required = true) final MultipartFile image,
			HttpSession session, HttpServletRequest servletRequest) {
		User user = (User) session.getAttribute(Constant.SESSION_USER);
		if (user != null) {
			try {
				this.imageService.uploadImage(user, image, servletRequest);
				return new ResponseMessageDto("Upload successfully");
			} catch (IllegalStateException e) {
				throw new MessageException(MessageExceptionErroCode.FILEUPLOAD);
			} catch (IOException e) {
				throw new MessageException(MessageExceptionErroCode.FILEUPLOAD);
			} catch (DataIntegrityViolationException dataIntegrityViolationException) {
				throw new FileExistException("Same file already exist");
			}
		} else {
			throw new AuthorizationException(Constant.ERROR_LOGIN_MESSAGE);
		}
	}

	// TODO shows
	@RequestMapping(value = "/show", method = RequestMethod.GET)
	public String showPre(HttpSession session, ModelMap modelMap) {
		User user = (User) session.getAttribute(Constant.SESSION_USER);
		if (user != null) {
			return Constant.VIEW_SHOW_LIST;
		} else {
			return "redirect:login";
		}
	}

	@RequestMapping(value = "/get-active-show", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody List<ShowDisplayDto> getActiveShow(
			HttpSession session, ModelMap modelMap) {
		User user = (User) session.getAttribute(Constant.SESSION_USER);
		if (user != null) {
			return this.showService.getAllActiveShow();
		} else {
			throw new AuthorizationException(Constant.ERROR_LOGIN_MESSAGE);
		}
	}

	@RequestMapping(value = "/edit-show/{id}", method = RequestMethod.GET)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody ShowDto editShowPre(@PathVariable("id") Long id,
			HttpSession session) {
		User user = (User) session.getAttribute(Constant.SESSION_USER);
		if (user != null) {
			return this.showService.getShow(id);
		} else {
			throw new AuthorizationException(Constant.ERROR_LOGIN_MESSAGE);
		}
	}

	@RequestMapping(value = "/edit-show/{id}", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody ResponseMessageDto editShowPost(
			@PathVariable("id") Long id, @RequestBody ShowDto show,
			HttpSession session) {
		User user = (User) session.getAttribute(Constant.SESSION_USER);
		if (user != null) {
			TimeZone timeZone = this.timeZoneService.getTimeZoneByName(show
					.getTimeZone());
			if (timeZone != null) {
				this.showService.updateShow(id, show, user, timeZone);
				return new ResponseMessageDto("Successfully updated");
			} else {
				ValidationErrorDTO validationErrorDTO = new ValidationErrorDTO();
				validationErrorDTO.addFieldError("timeZone", "Not valid");
				throw new FieldErrorException(validationErrorDTO);
			}
		} else {
			throw new AuthorizationException(Constant.ERROR_LOGIN_MESSAGE);
		}
	}

	@RequestMapping(value = "/delete-show/{id}", method = RequestMethod.DELETE, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody ResponseMessageDto deleteShow(
			@PathVariable("id") Long id, HttpSession session) {
		User user = (User) session.getAttribute(Constant.SESSION_USER);
		if (user != null) {
			this.showService.deleteShow(id);
			return new ResponseMessageDto("Successfully");
		} else {
			throw new AuthorizationException(Constant.ERROR_LOGIN_MESSAGE);
		}
	}

	@RequestMapping(value = "/get-time-zone", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody List<String> getTimeZone(HttpSession session) {
		User user = (User) session.getAttribute(Constant.SESSION_USER);
		if (user != null) {
			return this.timeZoneService.getAllTimeZoneName();
		} else {
			throw new AuthorizationException(Constant.ERROR_LOGIN_MESSAGE);
		}
	}

	@RequestMapping(value = "/add-show", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody ResponseMessageDto addShow(
			@Valid @RequestBody ShowDto show, HttpSession session) {
		User user = (User) session.getAttribute(Constant.SESSION_USER);
		if (user != null) {
			TimeZone timeZone = this.timeZoneService.getTimeZoneByName(show
					.getTimeZone());
			if (timeZone != null) {
				this.showService.saveShow(show, user, timeZone);
				return new ResponseMessageDto("save successfully");
			} else {
				ValidationErrorDTO validationErrorDTO = new ValidationErrorDTO();
				validationErrorDTO.addFieldError("timeZone", "Not valid");
				throw new FieldErrorException(validationErrorDTO);
			}
		} else {
			throw new AuthorizationException(Constant.ERROR_LOGIN_MESSAGE);
		}
	}

	/*
	 * @RequestMapping(value = "/show-list", method = RequestMethod.GET) public
	 * String showList(HttpSession session, ModelMap modelMap) { User user =
	 * (User) session.getAttribute(Constant.SESSION_USER); if (user != null) {
	 * modelMap.put("shows", this.showService.getAllActiveShow()); return
	 * Constant.VIEW_SHOW_LIST; } else { return "redirect:login"; } }
	 */
}
