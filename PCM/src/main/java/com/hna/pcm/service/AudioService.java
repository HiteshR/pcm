package com.hna.pcm.service;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.multipart.MultipartFile;

import com.hna.pcm.dto.AudioDto;
import com.hna.pcm.model.User;

public interface AudioService {
	public void uploadAudio(User user, MultipartFile multipartFile,
			String trackName, HttpServletRequest servletRequest)
			throws IllegalStateException, IOException;

	public List<AudioDto> getAudios();

	public void delete(Long id);
}
