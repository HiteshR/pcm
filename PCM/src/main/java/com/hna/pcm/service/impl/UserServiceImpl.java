package com.hna.pcm.service.impl;

import java.io.IOException;

import javax.annotation.Resource;
import javax.mail.MessagingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.hna.pcm.common.Constant;
import com.hna.pcm.dto.ChangePasswordDto;
import com.hna.pcm.dto.ForgotPasswordDto;
import com.hna.pcm.dto.LoginDto;
import com.hna.pcm.dto.ResetPasswordDTO;
import com.hna.pcm.dto.ValidationErrorDTO;
import com.hna.pcm.exception.AlreadyVerifiedException;
import com.hna.pcm.exception.AlreadyVerifiedException.AlreadyVerifiedExceptionType;
import com.hna.pcm.exception.AuthenticationException;
import com.hna.pcm.exception.AuthorizationException;
import com.hna.pcm.exception.FieldErrorException;
import com.hna.pcm.exception.NotFoundException;
import com.hna.pcm.exception.NotFoundException.NotFound;
import com.hna.pcm.exception.TokenHasExpiredException;
import com.hna.pcm.model.User;
import com.hna.pcm.model.VerificationToken;
import com.hna.pcm.model.VerificationToken.VerificationTokenType;
import com.hna.pcm.repository.UserRepository;
import com.hna.pcm.repository.VerificationTokenRepository;
import com.hna.pcm.service.SendMailService;
import com.hna.pcm.service.UserService;
import com.hna.pcm.util.DateUtil;

import freemarker.template.TemplateException;

@Service(value = "userService")
public class UserServiceImpl implements UserService {

	private static final Logger logger = LoggerFactory
			.getLogger(UserServiceImpl.class);

	@Resource
	private UserRepository userRepository;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncode;

	@Autowired
	private VerificationTokenRepository verificationTokenRepository;

	@Autowired
	private SendMailService sendMailService;

	@Transactional(rollbackFor = { Exception.class }, isolation = Isolation.READ_COMMITTED)
	public User login(LoginDto loginDto) {
		logger.info("login Start");
		User objUser = this.userRepository.getUserByEmail(loginDto.getEmail());
		if (objUser != null) {
			if (this.bCryptPasswordEncode.matches(loginDto.getPassword(),
					objUser.getPassword())) {
				return objUser;
			} else {
				throw new AuthenticationException();
			}
		} else {
			throw new NotFoundException(NotFound.UserNotFound);
		}
	}

	@Override
	@Transactional(isolation = Isolation.READ_COMMITTED, rollbackFor = { Exception.class })
	public void forgotPassword(ForgotPasswordDto forgotPasswordModel,
			String redirectUrl) throws MessagingException, IOException,
			TemplateException {

		User objUser = this.userRepository.getUserByEmail(forgotPasswordModel
				.getEmail());

		// check user available in data base or not
		if (null != objUser) {
			String verificationToken = this.verificationTokenRepository
					.getVerifactionTokenByEmail(objUser.getEmail(),
							VerificationTokenType.lostPassword);

			if (!StringUtils.hasText(verificationToken)) {
				// create verification token
				VerificationToken objVerificationToken = new VerificationToken(
						objUser, VerificationTokenType.lostPassword,
						Constant.LOST_PASSWORD_EMAIL_LINK_EXPIRE_TIME);
				objVerificationToken.setCreatedBy(objUser);
				objVerificationToken.setIsUsed(false);
				this.verificationTokenRepository.save(objVerificationToken);
				verificationToken = objVerificationToken.getToken();
			}

			// send mail
			this.sendMailService.sendForgotPasswordMail(objUser, redirectUrl,
					verificationToken);
		} else {
			// if no data found send error message
			throw new NotFoundException(NotFound.UserNotFound);
		}
	}

	@Override
	@Transactional(rollbackFor = { Exception.class })
	public void changePassword(ChangePasswordDto changePasswordDto, User user) {
		if (user != null) {
			if (this.bCryptPasswordEncode.matches(
					changePasswordDto.getCurrentPassword(), user.getPassword())) {
				if (!changePasswordDto.getCurrentPassword().equalsIgnoreCase(
						changePasswordDto.getConfirmPassword())) {
					user.setPassword(this.bCryptPasswordEncode
							.encode(changePasswordDto.getConfirmPassword()));
					user.setModifiedBy(user);
					user.setModifiedDate(DateUtil.getCurrentDate());
					// update
					this.userRepository.save(user);
				} else {
					ValidationErrorDTO validationErrorDTO = new ValidationErrorDTO();
					validationErrorDTO.addFieldError("newPassword",
							"Password cant be same as current.");
					throw new FieldErrorException(validationErrorDTO);
				}
			} else {
				ValidationErrorDTO validationErrorDTO = new ValidationErrorDTO();
				validationErrorDTO.addFieldError("currentPassword",
						"Password not match");
				throw new FieldErrorException(validationErrorDTO);
			}
		} else {
			throw new AuthorizationException(Constant.ERROR_LOGIN_MESSAGE);
		}

	}

	@Transactional(isolation = Isolation.READ_COMMITTED, rollbackFor = { Exception.class })
	@Override
	public void resetPassword(ResetPasswordDTO resetPasswordDto) {
		VerificationToken objVerificationToken = this.verificationTokenRepository
				.getVerifactionTokenByToken(resetPasswordDto.getToken(),
						VerificationTokenType.lostPassword);
		if (objVerificationToken != null) {
			if (objVerificationToken.getIsUsed()) {
				throw new AlreadyVerifiedException(
						AlreadyVerifiedExceptionType.Token);
			} else {
				if (objVerificationToken.hasExpired()) {
					throw new TokenHasExpiredException();
				} else {
					User objUser = objVerificationToken.getUser();
					if (objUser != null) {
						objUser.setPassword(this.bCryptPasswordEncode
								.encode(resetPasswordDto.getConfirmPassword()));
						objUser.setModifiedBy(objUser);
						objUser.setModifiedDate(DateUtil.getCurrentDate());
						// update
						this.userRepository.save(objUser);
						objVerificationToken.setIsUsed(true);
						// update
						this.verificationTokenRepository
								.save(objVerificationToken);
					} else {
						throw new NotFoundException(NotFound.TokenNotFound);
					}
				}
			}
		} else {
			throw new NotFoundException(NotFound.TokenNotFound);
		}
	}
}
