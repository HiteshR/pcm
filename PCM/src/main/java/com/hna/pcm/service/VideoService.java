package com.hna.pcm.service;

import com.hna.pcm.dto.VideoDto;
import com.hna.pcm.model.User;

public interface VideoService {
	public VideoDto getVideoLink();

	public void saveLink(VideoDto videoDto, User user);
}
