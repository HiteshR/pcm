package com.hna.pcm.service;

import java.util.List;

import com.hna.pcm.model.TimeZone;

public interface TimeZoneService {
	public List<String> getAllTimeZoneName();

	public TimeZone getTimeZoneByName(String name);
}
