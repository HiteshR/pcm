package com.hna.pcm.service.impl;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.hna.pcm.dto.ImageDto;
import com.hna.pcm.exception.NotFoundException;
import com.hna.pcm.exception.NotFoundException.NotFound;
import com.hna.pcm.model.Image;
import com.hna.pcm.model.User;
import com.hna.pcm.repository.ImageRepository;
import com.hna.pcm.service.ImageService;
import com.hna.pcm.util.DateUtil;

@Service(value = "imageService")
public class ImageServiceImpl implements ImageService {

	@Resource
	private ImageRepository imageRepository;

	private List<String> extensions;

	public ImageServiceImpl() {
		extensions = new ArrayList<String>();
		extensions.add("image/jpeg");
		extensions.add("image/png");
	}

	@Transactional(rollbackFor = { Exception.class }, isolation = Isolation.READ_COMMITTED)
	public void uploadImage(User user, MultipartFile multipartFile,
			HttpServletRequest servletRequest) throws IllegalStateException,
			IOException {

		validateImage(multipartFile);

		StringBuilder localImagePath = new StringBuilder();

		String webAppRoot = new File(servletRequest.getSession()
				.getServletContext().getRealPath("/")).getAbsolutePath();

		String fileName = multipartFile.getOriginalFilename();

		// Save name in database
		Image image = new Image();
		image.setCreatedDate(DateUtil.getCurrentDate());
		image.setCreatedBy(user);
		image.setName(fileName);
		// try {
		this.imageRepository.save(image);
		// } catch (DataIntegrityViolationException
		// dataIntegrityViolationException) {
		// throw new FileExistException("Same file already exist");
		// }

		// Save file to local directory
		localImagePath.delete(0, localImagePath.length()).append(webAppRoot)
				.append(File.separator).append("resources")
				.append(File.separator).append("images").append(File.separator)
				.append("slideshow").append(File.separator).append(fileName);
		multipartFile.transferTo(new File(localImagePath.toString()));
	}

	private void validateImage(MultipartFile image) {
		if (!extensions.contains(image.getContentType())) {
			throw new RuntimeException("Only JPG AND PNG images are accepted");
		}
	}

	@Transactional(rollbackFor = { Exception.class }, isolation = Isolation.READ_COMMITTED)
	public List<ImageDto> getImages() {
		return this.imageRepository.getAllImages();
	}

	@Transactional(rollbackFor = { Exception.class }, isolation = Isolation.READ_COMMITTED)
	public void delete(Long id) {
		Image image = this.imageRepository.findOne(id);
		if (image != null) {
			this.imageRepository.delete(image);
		} else
			throw new NotFoundException(NotFound.ImageNotFound);
	}
}
