package com.hna.pcm.service;

import java.io.IOException;

import javax.mail.MessagingException;

import com.hna.pcm.model.User;

import freemarker.template.TemplateException;

public interface SendMailService {

	public void sendForgotPasswordMail(User user, String redirectUrl,
			String token) throws MessagingException, IOException,
			TemplateException;

}
