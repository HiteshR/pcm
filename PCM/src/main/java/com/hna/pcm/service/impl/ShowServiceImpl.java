package com.hna.pcm.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.hna.pcm.dto.ShowDisplayDto;
import com.hna.pcm.dto.ShowDto;
import com.hna.pcm.exception.NotFoundException;
import com.hna.pcm.exception.NotFoundException.NotFound;
import com.hna.pcm.model.Show;
import com.hna.pcm.model.TimeZone;
import com.hna.pcm.model.User;
import com.hna.pcm.repository.ShowRepository;
import com.hna.pcm.service.ShowService;
import com.hna.pcm.util.DateUtil;

@Service(value = "showService")
public class ShowServiceImpl implements ShowService {

	@Resource
	private ShowRepository showRepository;

	@Transactional(rollbackFor = { Exception.class }, isolation = Isolation.READ_COMMITTED)
	public void saveShow(ShowDto showDto, User user, TimeZone timeZone) {
		Show show = new Show();
		show.setCity(showDto.getCity());
		show.setCreatedBy(user);
		show.setCreatedDate(DateUtil.getCurrentDate());
		show.setShowDate(DateUtil.getDateTimeWithZone(showDto.getDate(),
				"MM/dd/yyyy HH:mm", timeZone.getZone()));
		show.setDetail(showDto.getDetail());
		show.setVenue(showDto.getVenue());
		show.setTimeZone(timeZone);
		this.showRepository.save(show);
	}

	@Transactional(rollbackFor = { Exception.class }, isolation = Isolation.READ_COMMITTED)
	public List<ShowDisplayDto> getAllShow() {
		return this.showRepository.getAllShow();
	}

	@Transactional(rollbackFor = { Exception.class }, isolation = Isolation.READ_COMMITTED)
	public List<ShowDisplayDto> getAllActiveShow() {
		return this.showRepository.getAllActiveShow();
	}

	@Transactional(rollbackFor = { Exception.class }, isolation = Isolation.READ_COMMITTED)
	public void deleteShow(Long id) {
		Show show = this.showRepository.findOne(id);
		if (show != null) {
			this.showRepository.delete(show);
		} else
			throw new NotFoundException(NotFound.ShowFound);
	}

	@Transactional(rollbackFor = { Exception.class }, isolation = Isolation.READ_COMMITTED)
	public ShowDto getShow(Long id) {
		ShowDto showDto = this.showRepository.getShow(id);
		if (showDto != null) {
			return showDto;
		} else {
			throw new NotFoundException(NotFound.ShowFound);
		}
	}

	@Transactional(rollbackFor = { Exception.class }, isolation = Isolation.READ_COMMITTED)
	public void updateShow(Long id, ShowDto showDto, User user,
			TimeZone timeZone) {
		Show show = this.showRepository.findOne(id);
		if (show != null) {
			show.setCity(showDto.getCity());
			show.setModifiedBy(user);
			show.setModifiedDate(DateUtil.getCurrentDate());
			// show.setShowDate(showDto.getDate());
			show.setShowDate(DateUtil.getDateTimeWithZone(showDto.getDate(),
					"MM/dd/yyyy HH:mm", timeZone.getZone()));
			show.setDetail(showDto.getDetail());
			show.setVenue(showDto.getVenue());
			show.setTimeZone(timeZone);
			this.showRepository.save(show);
		} else {
			throw new NotFoundException(NotFound.ShowFound);
		}
	}
}
