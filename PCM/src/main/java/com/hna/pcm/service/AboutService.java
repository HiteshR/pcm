package com.hna.pcm.service;

import com.hna.pcm.model.About;
import com.hna.pcm.model.User;

public interface AboutService {
	public About getContent();

	public void saveContent(String content, User user);
}
