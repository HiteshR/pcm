package com.hna.pcm.service.impl;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.hna.pcm.model.About;
import com.hna.pcm.model.User;
import com.hna.pcm.repository.AboutRepository;
import com.hna.pcm.service.AboutService;
import com.hna.pcm.util.DateUtil;

@Service(value = "aboutService")
public class AboutServiceImpl implements AboutService {
	private static final Logger logger = LoggerFactory
			.getLogger(AboutServiceImpl.class);

	@Resource
	private AboutRepository aboutRepository;

	@Transactional(rollbackFor = { Exception.class }, isolation = Isolation.READ_COMMITTED)
	public About getContent() {
		logger.info("getContent");
		return this.aboutRepository.getContent();
	}

	@Transactional(rollbackFor = { Exception.class }, isolation = Isolation.READ_COMMITTED)
	public void saveContent(String content, User user) {
		logger.info("saveContent");
		About about = this.aboutRepository.getContent();
		if (about != null) {
			about.setContent(content);
			about.setModifiedDate(DateUtil.getCurrentDate());
			about.setModifiedBy(user);
			this.aboutRepository.save(about);
		} else {
			about = new About();
			about.setContent(content);
			about.setCreatedDate(DateUtil.getCurrentDate());
			about.setActive(true);
			about.setCreatedBy(user);
			this.aboutRepository.save(about);
		}
	}
}
