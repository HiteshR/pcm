package com.hna.pcm.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.hna.pcm.model.TimeZone;
import com.hna.pcm.repository.TimeZoneRepository;
import com.hna.pcm.service.TimeZoneService;

@Service(value = "timeZoneService")
public class TimeZoneServiceImpl implements TimeZoneService {
	private static final Logger logger = LoggerFactory
			.getLogger(TimeZoneServiceImpl.class);

	@Resource
	private TimeZoneRepository timeZoneRepository;

	@Transactional(rollbackFor = { Exception.class }, isolation = Isolation.READ_COMMITTED)
	public List<String> getAllTimeZoneName() {
		logger.info("getAllTimeZoneName");
		return this.timeZoneRepository.getAllTimeZoneName();
	}

	@Transactional(rollbackFor = { Exception.class }, isolation = Isolation.READ_COMMITTED)
	public TimeZone getTimeZoneByName(String name) {
		logger.info("getTimeZoneByName");
		return this.timeZoneRepository.getTimeZoneByName(name);
	}
}
