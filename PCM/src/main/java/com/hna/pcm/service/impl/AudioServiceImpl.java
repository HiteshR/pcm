package com.hna.pcm.service.impl;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.hna.pcm.dto.AudioDto;
import com.hna.pcm.exception.ExtentionException;
import com.hna.pcm.exception.NotFoundException;
import com.hna.pcm.exception.NotFoundException.NotFound;
import com.hna.pcm.model.Audio;
import com.hna.pcm.model.User;
import com.hna.pcm.repository.AudioRepository;
import com.hna.pcm.service.AudioService;
import com.hna.pcm.util.DateUtil;

@Service(value = "audioService")
public class AudioServiceImpl implements AudioService {

	@Resource
	private AudioRepository audioRepository;

	private List<String> extensions;

	public AudioServiceImpl() {
		extensions = new ArrayList<String>();
		extensions.add("audio/mpeg");
	}

	@Transactional(rollbackFor = { Exception.class }, isolation = Isolation.READ_COMMITTED)
	public void uploadAudio(User user, MultipartFile multipartFile,
			String trackName, HttpServletRequest servletRequest)
			throws IllegalStateException, IOException {

		validateImage(multipartFile);

		StringBuilder localImagePath = new StringBuilder();

		String webAppRoot = new File(servletRequest.getSession()
				.getServletContext().getRealPath("/")).getAbsolutePath();

		String fileName = multipartFile.getOriginalFilename();

		// Save name in database
		Audio audio = new Audio();
		audio.setCreatedDate(DateUtil.getCurrentDate());
		audio.setCreatedBy(user);
		audio.setTrackName(trackName);
		audio.setFileName(fileName);
		this.audioRepository.save(audio);

		// Save file to local directory
		localImagePath.delete(0, localImagePath.length()).append(webAppRoot)
				.append(File.separator).append("resources")
				.append(File.separator).append("music").append(File.separator)
				.append(fileName);
		multipartFile.transferTo(new File(localImagePath.toString()));
	}

	private void validateImage(MultipartFile image) {
		if (!extensions.contains(image.getContentType())) {
			throw new ExtentionException("Only MP3 files is accepted");
		}
	}

	public List<AudioDto> getAudios() {
		return this.audioRepository.getAllAudios();
	}

	@Transactional(rollbackFor = { Exception.class }, isolation = Isolation.READ_COMMITTED)
	public void delete(Long id) {
		Audio audio = this.audioRepository.findOne(id);
		if (audio != null) {
			this.audioRepository.delete(audio);
		} else
			throw new NotFoundException(NotFound.AudioNotFound);
	}
}
