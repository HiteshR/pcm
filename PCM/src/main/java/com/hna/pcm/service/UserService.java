package com.hna.pcm.service;

import java.io.IOException;

import javax.mail.MessagingException;

import com.hna.pcm.dto.ChangePasswordDto;
import com.hna.pcm.dto.ForgotPasswordDto;
import com.hna.pcm.dto.LoginDto;
import com.hna.pcm.dto.ResetPasswordDTO;
import com.hna.pcm.model.User;

import freemarker.template.TemplateException;

public interface UserService {
	public User login(LoginDto loginDto);

	public void changePassword(ChangePasswordDto changePasswordDto, User user);

	public void resetPassword(ResetPasswordDTO resetPasswordDto);

	public void forgotPassword(ForgotPasswordDto forgotPasswordModel,
			String redirectUrl) throws MessagingException, IOException,
			TemplateException;
}
