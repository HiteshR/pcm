package com.hna.pcm.service.impl;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.hna.pcm.dto.VideoDto;
import com.hna.pcm.model.User;
import com.hna.pcm.model.Video;
import com.hna.pcm.repository.VideoRepository;
import com.hna.pcm.service.VideoService;
import com.hna.pcm.util.DateUtil;

@Service(value = "videoService")
public class VideoServiceImpl implements VideoService {
	private static final Logger logger = LoggerFactory
			.getLogger(VideoServiceImpl.class);

	@Resource
	private VideoRepository videoRepository;

	@Transactional(rollbackFor = { Exception.class }, isolation = Isolation.READ_COMMITTED)
	public VideoDto getVideoLink() {
		logger.info("getContent");
		return this.videoRepository.getVideoLink();
	}

	@Transactional(rollbackFor = { Exception.class }, isolation = Isolation.READ_COMMITTED)
	public void saveLink(VideoDto videoDto, User user) {
		logger.info("saveContent");
		Video video = this.videoRepository.getVideo();
		if (video != null) {
			video.setLink(videoDto.getLink());
			video.setModifiedDate(DateUtil.getCurrentDate());
			video.setModifiedBy(user);
			this.videoRepository.save(video);
		} else {
			video = new Video();
			video.setLink(videoDto.getLink());
			video.setCreatedDate(DateUtil.getCurrentDate());
			video.setActive(true);
			video.setCreatedBy(user);
			this.videoRepository.save(video);
		}
	}
}
