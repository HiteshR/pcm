package com.hna.pcm.service.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.mail.MessagingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hna.pcm.common.Constant;
import com.hna.pcm.model.User;
import com.hna.pcm.service.ApplicationMailerService;
import com.hna.pcm.service.SendMailService;

import freemarker.template.TemplateException;

@Service(value = "sendMailService")
public class SendMailServiceImpl implements SendMailService {

	@Autowired
	private ApplicationMailerService applicationMailerService;

	private static final Logger logger = LoggerFactory
			.getLogger(SendMailServiceImpl.class);

	@Override
	public void sendForgotPasswordMail(User user, String redirectUrl,
			String token) throws MessagingException, IOException,
			TemplateException {
		Map<String, Object> objMapTemplet = null;
		StringBuilder objSB = null;
		try {
			logger.info("sendForgotPasswordMail start");
			// get user data from list
			objMapTemplet = new HashMap<String, Object>();
			objMapTemplet.put("name", user.getEmail());
			objSB = new StringBuilder();
			objSB.delete(0, objSB.length()).append(redirectUrl).append("?key=")
					.append(token);
			objMapTemplet.put("resetPasswordUrl", objSB.toString());
			Map<String, String> images = new HashMap<String, String>();
			images.put("orsEmailLogo", "orsEmailLogo.png");
			images.put("emailResetPW", "emailResetPW.png");

			// send mail to user
			this.applicationMailerService.sendTemplateMail(user.getEmail(),
					Constant.EMAIL_HEADER_RESET_PASSWORD,
					Constant.TEMPLATE_EMAIL_RESET_PASSWORD, objMapTemplet,
					images);
			logger.info("sendForgotPasswordMail end");
		} finally {
			objMapTemplet = null;
			objSB = null;
		}
	}

}
