package com.hna.pcm.service;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.multipart.MultipartFile;

import com.hna.pcm.dto.ImageDto;
import com.hna.pcm.model.User;

public interface ImageService {
	public void uploadImage(User user, MultipartFile multipartFile,
			HttpServletRequest servletRequest) throws IllegalStateException,
			IOException;

	public List<ImageDto> getImages();

	public void delete(Long id);
}
