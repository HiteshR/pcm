package com.hna.pcm.service;

import java.util.List;

import com.hna.pcm.dto.ShowDisplayDto;
import com.hna.pcm.dto.ShowDto;
import com.hna.pcm.model.TimeZone;
import com.hna.pcm.model.User;

public interface ShowService {
	public void saveShow(ShowDto showDto, User user, TimeZone timeZone);

	public List<ShowDisplayDto> getAllShow();

	public List<ShowDisplayDto> getAllActiveShow();

	public void deleteShow(Long id);

	public ShowDto getShow(Long id);

	public void updateShow(Long id, ShowDto showDto, User user,
			TimeZone timeZone);
}
