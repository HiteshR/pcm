package com.hna.pcm.util;

import java.util.Calendar;
import java.util.Date;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.hna.pcm.exception.DateException;
import com.hna.pcm.exception.DateException.DateExceptionErroCode;

public class DateUtil {

	public static Date getCurrentDate() {
		return new Date();
	}

	public static Integer dateDifference(Date startDate, Date endDate) {
		if (startDate.before(endDate)) {
			return Days.daysBetween(new LocalDate(startDate),
					new LocalDate(endDate)).getDays();
		} else {
			throw new DateException(
					DateExceptionErroCode.EndDateGreaterException);
		}
	}

	public static Date getDate(String date, String dateFormat) {
		DateTimeFormatter objDateFormat = null;
		LocalDate objLocalDate = null;
		try {
			objDateFormat = DateTimeFormat.forPattern(dateFormat);
			objLocalDate = LocalDate.parse(date, objDateFormat);
			return objLocalDate.toDate();
		} catch (IllegalArgumentException exception) {
			throw new DateException(
					DateExceptionErroCode.PatternIsInvalidException);
		}
	}

	public static Date getDateTime(String date, String dateFormat) {
		DateTimeFormatter objDateFormat = null;
		LocalDateTime objLocalDateTime = null;
		try {
			objDateFormat = DateTimeFormat.forPattern(dateFormat);
		} catch (IllegalArgumentException exception) {
			throw new DateException(
					DateExceptionErroCode.PatternIsInvalidException);
		}
		try {
			objLocalDateTime = objDateFormat.parseLocalDateTime(date);
			return objLocalDateTime.toDate();
		} catch (UnsupportedOperationException exception) {
			throw new DateException(
					DateExceptionErroCode.PatternIsInvalidException);
		} catch (IllegalArgumentException exception) {
			throw new DateException(
					DateExceptionErroCode.TextToParseIsInvalidException);
		}
	}

	public static Date getDateTimeWithZone(String date, String dateFormat,
			String zoneOffSet) {
		DateTimeFormatter objDateFormat = null;
		DateTime objLocalDateTime = null;
		StringBuilder objSB = null;
		try {
			objSB = new StringBuilder();
			objSB.delete(0, objSB.length()).append(dateFormat)
					.append(" 'UTC'Z");
			objDateFormat = DateTimeFormat.forPattern(objSB.toString());
		} catch (IllegalArgumentException exception) {
			throw new DateException(
					DateExceptionErroCode.PatternIsInvalidException);
		}
		try {
			objSB.delete(0, objSB.length()).append(date).append(" UTC")
					.append(zoneOffSet);
			objLocalDateTime = objDateFormat.withOffsetParsed()
					.parseDateTime(objSB.toString());
			return objLocalDateTime.toDate();
		} catch (UnsupportedOperationException exception) {
			throw new DateException(
					DateExceptionErroCode.PatternIsInvalidException);
		} catch (IllegalArgumentException exception) {
			throw new DateException(
					DateExceptionErroCode.TextToParseIsInvalidException);
		}
	}

	public static String getDate(Date date, String dateFormat) {
		try {
			DateTimeFormatter objDateFormat = DateTimeFormat
					.forPattern(dateFormat);
			DateTime objDateTime = new DateTime(date);
			return objDateFormat.print(objDateTime);
		} catch (IllegalArgumentException exception) {
			throw new DateException(
					DateExceptionErroCode.PatternIsInvalidException);
		}
	}

	public static Date getAddedDate(int seconds, int minutes, int hours) {
		DateTime objDateTime = null;
		objDateTime = new DateTime();
		objDateTime = objDateTime.plusSeconds(seconds);
		objDateTime = objDateTime.plusMinutes(minutes);
		objDateTime = objDateTime.plusHours(hours);
		return objDateTime.toDate();
	}

	public static Date getAddedDate(int days) {
		DateTime objDateTime = null;
		objDateTime = new DateTime();
		objDateTime = objDateTime.plusDays(days);
		return objDateTime.toDate();
	}

	public static Date currentDateWithoutTime() {
		Date currentDate = null;
		Calendar newDate = null;
		currentDate = new Date();
		newDate = Calendar.getInstance();
		newDate.setLenient(false);
		newDate.setTime(currentDate);
		newDate.set(Calendar.HOUR_OF_DAY, 0);
		newDate.set(Calendar.MINUTE, 0);
		newDate.set(Calendar.SECOND, 0);
		newDate.set(Calendar.MILLISECOND, 0);
		currentDate = newDate.getTime();
		return currentDate;
	}

}
